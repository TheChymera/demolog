from labbookdb.db.add import add_generic
db_location = 'meta.db'
#Incubations
add_generic(db_location, parameters={"CATEGORY":"Incubation", "revolutions_per_minute":1000, "duration":60, "temperature":55, "movement":"shake", "duration_unit_id":"MeasurementUnit:code.min", "temperature_unit_id":"MeasurementUnit:code.dC"})
add_generic(db_location, parameters={"CATEGORY":"Incubation", "revolutions_per_minute":1400, "duration":1, "temperature":95, "movement":"shake", "duration_unit_id":"MeasurementUnit:code.min", "temperature_unit_id":"MeasurementUnit:code.dC"})
add_generic(db_location, parameters={"CATEGORY":"Incubation", "revolutions_per_minute":0, "duration":12, "temperature":95, "duration_unit_id":"MeasurementUnit:code.min", "temperature_unit_id":"MeasurementUnit:code.dC"})
add_generic(db_location, parameters={"CATEGORY":"Incubation", "revolutions_per_minute":0, "duration":15, "temperature":0, "duration_unit_id":"MeasurementUnit:code.min", "temperature_unit_id":"MeasurementUnit:code.dC"})
add_generic(db_location, parameters={"CATEGORY":"Incubation", "revolutions_per_minute":14000, "duration":5, "movement":"centrifuge", "duration_unit_id":"MeasurementUnit:code.min", "temperature_unit_id":"MeasurementUnit:code.dC"})

#DNA EXTRACTION PROTOCOLS
add_generic(db_location, parameters={"CATEGORY":"DNAExtractionProtocol", "code":"EPDqEP", "name":"Ear Punch DNA quick-Extraction Protocol", "digestion_buffer_volume":20, "proteinase_volume":2, "lysis_buffer_volume":200,
	"volume_unit_id":"MeasurementUnit:code.mul",
	"digestion_buffer_id":"Solution:code.EPqEB",
	"proteinase_id":"Solution:code.pk",
	"lysis_buffer_id":"Solution:code.mpw",
	"digestion_id":"Incubation:revolutions_per_minute.1000&&duration.60",
	"lysis_id":"Incubation:revolutions_per_minute.1400&&duration.1",
	"inactivation_id":"Incubation:revolutions_per_minute.0&&duration.12",
	"cooling_id":"Incubation:revolutions_per_minute.0&&duration.15",
	"centrifugation_id":"Incubation:revolutions_per_minute.14000&&duration.5",
	"authors":["Operator:code.YPI"],
	})
