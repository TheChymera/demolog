from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'
#injections+implants

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,5,14,10", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6255",
	"weight":32.5,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,4,5,14,10", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6255",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1.5&&virus_id.Virus:code.dflox.hChR2.YFP&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_dura",
		],
	"irregularities":[
		"Irregularity:description.removed and reinserted clogged injector (pre injection)",
		"Irregularity:description.injected volume fast (in 5')",
		]
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,5,15,40", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6255",
	"weight":32.6,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,6,18,0", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6255",
	"weight":30.9,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,7,14,30", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6255",
	"weight":31.2,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,5,16,15", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5698",
	"weight":39.6,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,4,5,16,15", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5698",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1.5&&virus_id.Virus:code.dflox.hChR2.YFP&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_dura",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,5,17,32", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5698",
	"weight":39.7,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,6,18,0", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5698",
	"weight":37.7,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,7,14,30", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5698",
	"weight":37.9,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,5,17,40", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5699",
	"weight":32.7,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,4,5,17,40", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5699",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1.5&&virus_id.Virus:code.dflox.hChR2.YFP&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_dura",
		],
	"irregularities":[
		"Irregularity:description.approximately 400 microlitres bleeding from injection site",
		"Irregularity:description.completely clear reflux",
		]
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,5,18,45", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5699",
	"weight":32.8,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,6,18,0", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5699",
	"weight":31.8,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,7,14,30", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5699",
	"weight":32.8,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,5,18,54", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5700",
	"weight":39.1,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,4,5,18,54", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5700",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1.3&&virus_id.Virus:code.dflox.hChR2.YFP&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_dura_shallow",
		],
	"irregularities":[
		"Irregularity:description.removed and reinserted injector (mid injection)",
		]
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,5,20,10", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5700",
	"weight":39.3,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,6,18,0", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5700",
	"weight":38.8,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,7,14,30", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5700",
	"weight":38.8,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,5,22,15", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6261",
	"weight":24.2,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,4,5,22,15", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6261",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1.5&&virus_id.Virus:code.dflox.hChR2.YFP&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_dura_shallow",
		],
	"irregularities":[
		"Irregularity:description.injected volume very fast (in 1')",
		"Irregularity:description.removed and reinserted clogged injector (pre injection)",
		"Irregularity:description.bloody reflux",
		]
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,5,23,37", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6261",
	"weight":24.3,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,6,18,0", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6261",
	"weight":23.7,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,7,14,30", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6261",
	"weight":24.2,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,5,23,45", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6262",
	"weight":25.0,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,4,5,23,45", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6262",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1.5&&virus_id.Virus:code.dflox.hChR2.YFP&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_skull",
		],
	"irregularities":[
		"Irregularity:description.injected volume fast (in 5')",
		]
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,6,0,50", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6262",
	"weight":25.1,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,6,18,0", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6262",
	"weight":23.0,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,7,14,30", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6262",
	"weight":24.2,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,6,1,0", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5692",
	"weight":26.0,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,4,6,1,0", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5692",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1.5&&virus_id.Virus:code.dflox.hChR2.YFP&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_skull",
		],
	"irregularities":[
		"Irregularity:description.injected volume very fast (in 1')",
		"Irregularity:description.approximately 100 microlitres bleeding from injection site",
		]
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,6,2,10", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5692",
	"weight":26.0,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,6,18,0", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5692",
	"weight":24.8,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,7,14,30", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5692",
	"weight":24.8,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,6,2,20", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5694",
	"weight":27.1,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,4,6,2,20", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5694",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1.5&&virus_id.Virus:code.dflox.hChR2.YFP&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_skull",
		],
	"irregularities":[
		"Irregularity:description.approximately 200 microlitres bleeding from injection site",
		"Irregularity:description.completely clear reflux",
		]
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,6,3,25", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5694",
	"weight":27.1,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,6,18,0", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5694",
	"weight":26.2,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,7,14,30", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5694",
	"weight":25.2,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Observation", "date":"2017,4,7,14,30", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5694",
	"physiology":"blind on left eye",
	})


# VTA

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,6,12,50", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6258",
	"weight":30.1,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,4,6,12,50", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6258",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1.5&&virus_id.Virus:code.dflox.hChR2.YFP&&stereotactic_target_id.OrthogonalStereotacticTarget:code.vta_dura",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,6,13,40", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6258",
	"weight":30.2,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,7,14,30", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6258",
	"weight":28.3,"weight_unit_id":"MeasurementUnit:code.g"
	})

# ###################################### implants ###########################

#DR

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,19,13,40", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6255",
	"weight":31.5 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,4,19,13,40", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6255",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.3.3&#&transmittance.78&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_impl",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,19,14,35", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6255",
	"weight":31.5 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,20,14,30", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6255",
	"weight":30.1 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,21,18", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6255",
	"weight":30.2 ,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,19,14,55", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5698",
	"weight":38.1 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,4,19,14,55", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5698",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.3.3&#&transmittance.100&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_impl",
		],
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,19,15,45", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5699",
	"weight":28.7 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,4,19,15,45", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5699",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.3.3&#&transmittance.95&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_impl",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,19,16,20", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5699",
	"weight":28.6 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,20,14,30", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5699",
	"weight":28.2 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,21,18", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5699",
	"weight":26.2 ,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,19,16,25", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5700",
	"weight":37.7 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,4,19,16,25", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5700",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.3.3&#&transmittance.100&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_impl",
		],
	"irregularities":[
		"Irregularity:description.removed and reinserted implant",
		]
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,19,17,30", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5700",
	"weight":37.6 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,20,14,30", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5700",
	"weight":36.8 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,21,18", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5700",
	"weight":36.1 ,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,19,18,0", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6261",
	"weight":24.1 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,4,19,18,0", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6261",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.3.3&#&transmittance.90&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_impl",
		],
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,19,18,40", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6262",
	"weight":25.2 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,4,19,18,40", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6262",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.3.3&#&transmittance.100&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_impl",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,19,19,10", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6262",
	"weight":25.3 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,20,14,30", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6262",
	"weight":24.9 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,21,18", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6262",
	"weight":24.5 ,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,19,19,15", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5692",
	"weight":25.1 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2016,9,22,11", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5692",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.3.3&#&transmittance.98&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_impl",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,19,19,50", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5692",
	"weight":25.1 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,20,14,30", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5692",
	"weight":24.6 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,21,18", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5692",
	"weight":25.3 ,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,19,19,55", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5694",
	"weight":26.1 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,4,19,19,55", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5694",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.3.3&#&transmittance.98&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_impl",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,19,20,30", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5694",
	"weight":26.3 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,20,14,30", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5694",
	"weight":24.1 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,21,18", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5694",
	"weight":24.5 ,"weight_unit_id":"MeasurementUnit:code.g"
	})

# VTA

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,20,16,10", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6258",
	"weight":29.3 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,4,20,16,10", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6258",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.4&#&transmittance.95&&stereotactic_target_id.OrthogonalStereotacticTarget:code.vta_impl",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,20,16,45", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6258",
	"weight":29.4 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
