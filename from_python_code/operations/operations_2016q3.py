from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'
#injections+implants

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,9,22,23", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2903",
	"weight":29.8 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2016,9,22,11", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2903",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.2.6&#&transmittance.25&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_impl",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,9,22,23,59", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2903",
	"weight":30.3 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,9,23,22", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2903",
	"weight":29.3 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,9,24,15", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2903",
	"weight":29.5 ,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,9,22,21", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2906",
	"weight":28 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2016,9,22,21", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2906",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1&&virus_id.Virus:code.dflox.hChR2.mCherry&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_skull_perpendicular",
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.3.3&#&transmittance.70&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_impl",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,9,22,22", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2906",
	"weight":28.3 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,9,23,22", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2906",
	"weight":25.4 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,9,24,15", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2906",
	"weight":26.6 ,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,9,22,22", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2905",
	"weight":30.8 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2016,9,22,22", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2905",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1&&virus_id.Virus:code.dflox.hChR2.mCherry&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_skull_perpendicular",
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.3.3&#&transmittance.65&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_impl",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,9,22,23", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2905",
	"weight":31.2 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,9,23,22", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2905",
	"weight":29.5 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,9,24,15", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2905",
	"weight":29.8 ,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,9,22,20", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2904",
	"weight":30.1 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2016,9,22,20", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2904",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1&&virus_id.Virus:code.dflox.hChR2.mCherry&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_skull_perpendicular",
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.3.3&#&transmittance.70&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_impl",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,9,22,21", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2904",
	"weight":30.3 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,9,23,22", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2904",
	"weight":28.1 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,9,24,15", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2904",
	"weight":28.9 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
