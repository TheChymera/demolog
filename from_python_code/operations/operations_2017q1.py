from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'
#injections+implants

add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,3,4,10,30", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5682",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1.5&&virus_id.Virus:code.dflox.hChR2.YFP&&stereotactic_target_id.OrthogonalStereotacticTarget:code.vta_dura",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,3,4,12", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5683",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1.5&&virus_id.Virus:code.dflox.hChR2.YFP&&stereotactic_target_id.OrthogonalStereotacticTarget:code.vta_dura",
		],
	})

add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,3,4,16", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5685",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1.5&&virus_id.Virus:code.dflox.hChR2.YFP&&stereotactic_target_id.OrthogonalStereotacticTarget:code.vta_dura",
		],
	})

add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,3,20,13", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5682",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.4&#&transmittance.90&&stereotactic_target_id.OrthogonalStereotacticTarget:code.vta_impl",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,3,20,15", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5685",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.4&#&transmittance.90&&stereotactic_target_id.OrthogonalStereotacticTarget:code.vta_impl",
		],
	"irregularities":["Irregularity:description.eroded skull",]
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,3,20,16", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5683",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.4&#&transmittance.90&&stereotactic_target_id.OrthogonalStereotacticTarget:code.vta_impl",
		],
	"irregularities":["Irregularity:description.eroded skull",]
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2017,3,22,16", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5684",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.4&#&transmittance.95&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_5684",
		],
	})
