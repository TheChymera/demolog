from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'
#injections+implants

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,12,15", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2886",
	"weight":22.4 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2016,10,12,15", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2886",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1&&virus_id.Virus:code.dflox.hChR2.YFP&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_skull_perpendicular",
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.3.3&#&transmittance.85&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_impl",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,12,17", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2886",
	"weight":22.8 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,13,10", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2886",
	"weight":21.6 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,14,13", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2886",
	"weight":21.9 ,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,12,17", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2885",
	"weight":21.1 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2016,10,12,17", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2885",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1&&virus_id.Virus:code.dflox.hChR2.YFP&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_skull_perpendicular",
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.3.3&#&transmittance.70&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_impl",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,12,18", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2885",
	"weight":21.3 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,13,10", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2885",
	"weight":20.6 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,14,13", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2885",
	"weight":22.6 ,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,12,14", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2888",
	"weight":22.4 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2016,10,12,14", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2888",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1&&virus_id.Virus:code.dflox.hChR2.YFP&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_skull_perpendicular",
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.3.3&#&transmittance.65&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_impl",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,12,15", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2888",
	"weight":23.2 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,13,10", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2888",
	"weight":21.7 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,14,13", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2888",
	"weight":22.2 ,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,11,17", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2883",
	"weight":31.9 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2016,10,11,17", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2883",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1&&virus_id.Virus:code.dflox.hChR2.YFP&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_skull_perpendicular",
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.3.3&#&transmittance.80&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_impl",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,11,19", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2883",
	"weight":31.8 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,12,17", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2883",
	"weight":31.1 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,13,10", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2883",
	"weight":32.6 ,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,11,16", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2884",
	"weight":30.4 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2016,10,11,16", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2884",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1&&virus_id.Virus:code.dflox.hChR2.YFP&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_skull_perpendicular",
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.3.3&#&transmittance.80&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_impl",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,11,18", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2884",
	"weight":30.2 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,12,17", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2884",
	"weight":29.7 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,13,10", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2884",
	"weight":29.7 ,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,11,16", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2912",
	"weight":28.1 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"Operation", "date":"2016,10,11,16", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2912",
	"anesthesia_id":"AnesthesiaProtocol:code.op",
	"protocols":[
		"VirusInjectionProtocol:amount.1&&virus_id.Virus:code.dflox.hChR2.YFP&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_skull_perpendicular",
		"OpticFiberImplantProtocol:optic_fiber_implant_id.OpticFiberImplant:length.3.3&#&transmittance.90&&stereotactic_target_id.OrthogonalStereotacticTarget:code.dr_impl",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,11,18", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2912",
	"weight":28.5 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,12,17", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2912",
	"weight":26.9 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2016,10,13,10", "operator_id":"Operator:code.ExCol",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.UZH/iRATS&#&identifier.M2912",
	"weight":25.7 ,"weight_unit_id":"MeasurementUnit:code.g"
	})
