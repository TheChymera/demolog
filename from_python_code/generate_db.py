#!/usr/bin/env python

import os
import time
import datetime as dt
from labbookdb.db.add import add_generic, append_parameter

db_location = 'meta.db'

if os.path.isfile(db_location):
	modified_time = os.path.getmtime(db_location)
	time_stamp =  dt.datetime.fromtimestamp(modified_time).isoformat()
	os.rename(db_location,"."+db_location+"_"+time_stamp)

import universal
import inventory
import common
import protocols
import cages
from animals import *
from behaviour import *
from cages import *
from deaths import *
from drinking import *
from fmri import *
from histology import *
from measurements import *
from operations import *
from treatments import *
import non_fmri

os.chmod(db_location, 0o664)
