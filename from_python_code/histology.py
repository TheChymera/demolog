from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'
add_generic(db_location, parameters={"CATEGORY":"FluorescentMicroscopyMeasurement",
"date":"2016,11,17",
"light_source":"LED",
"stimulation_wavelength":200,
"imaged_wavelength":200,
"exposure":2,
"data":"~/src/demolog/data/histology/drn/yfp/w1/",
"biopsy_id":"BrainBiopsy:start_date.2016,11,14,16&&animal_id.Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&##&identifier.5670"
})
add_generic(db_location, parameters={"CATEGORY":"FluorescentMicroscopyMeasurement",
"date":"2016,11,17",
"light_source":"LED",
"stimulation_wavelength":200,
"imaged_wavelength":200,
"exposure":2,
"data":"~/src/demolog/data/histology/drn/yfp/w2/",
"biopsy_id":"BrainBiopsy:start_date.2016,11,14,16&&animal_id.Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&##&identifier.5669"
})
