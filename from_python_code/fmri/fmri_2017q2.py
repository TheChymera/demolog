from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,11,12,33", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5682",
	"weight":29.2,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"FMRIMeasurement", "date":"2017,4,11,12,3,58", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5682",
	"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
	"stimulations":[
		"StimulationProtocol:code.chr_longSOA",
		],
	"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
	"temperature":34.8,
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,4,11,13,54", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5683",
	"weight":30.2,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"FMRIMeasurement", "date":"2017,4,11,13,54,38", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5683",
	"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
	"stimulations":[
		"StimulationProtocol:code.chr_longSOA",
		],
	"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
	"temperature":36.5,
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,12,17,49", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6255",
	"weight":30.2,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"FMRIMeasurement", "date":"2017,5,12,17,49,2", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6255",
	"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
	"stimulations":[
		"StimulationProtocol:code.chr_longSOA",
		],
	"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
	"temperature":36.2,
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,19,13,0", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6255",
	"weight":30.9,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,12,19,16", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5699",
	"weight":28.7,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"FMRIMeasurement", "date":"2017,5,12,19,16,40", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5699",
	"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
	"stimulations":[
		"StimulationProtocol:code.chr_longSOA",
		],
	"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
	"temperature":35.7,
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,19,13,0", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5699",
	"weight":28.0,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,12,20,36", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5700",
	"weight":39.5,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"FMRIMeasurement", "date":"2017,5,12,20,36,3", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5700",
	"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
	"stimulations":[
		"StimulationProtocol:code.chr_longSOA",
		],
	"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
	"temperature":36,
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,19,13,0", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5700",
	"weight":38.0,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,12,20,36", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5692",
	"weight":27.6,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"FMRIMeasurement", "date":"2017,5,12,20,36,3", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5692",
	"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
	"stimulations":[
		"StimulationProtocol:code.chr_longSOA",
		],
	"irregularities":[
		"Irregularity:description.ICA failed to indicate response to stimulus",
		],
	"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
	"temperature":36.6,
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,19,13,0", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5692",
	"weight":27.0,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,12,23,38", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5694",
	"weight":26.8,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"FMRIMeasurement", "date":"2017,5,12,23,38,14", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5694",
	"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
	"stimulations":[
		"StimulationProtocol:code.chr_longSOA",
		],
	"irregularities":[
		"Irregularity:description.injected saline into failed cannulation site"
		],
	"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
	"temperature":36,
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,19,13,0", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5694",
	"weight":26.4,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,13,1,11", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6262",
	"weight":25.5,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"FMRIMeasurement", "date":"2017,5,13,1,11", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6262",
	"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
	"stimulations":[
		"StimulationProtocol:code.chr_longSOA",
		],
	"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
	"temperature":36.1,
	})
add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,19,13,0", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6262",
	"weight":24.9,"weight_unit_id":"MeasurementUnit:code.g"
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,13,16,53", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5682",
	"weight":29.2,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"FMRIMeasurement", "date":"2017,5,13,16,53,44", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5682",
	"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
	"irregularities":[
		"Irregularity:description.agitated during induction",
		"Irregularity:description.tail too short, unable to cannulate",
		],
	"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
	"temperature":34.8,
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,13,17,34", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5683",
	"weight":29.2,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"FMRIMeasurement", "date":"2017,5,13,17,34,49", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5683",
	"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
	"irregularities":[
		"Irregularity:description.stimulation apparatus failed, unable to deliver stimulation",
		],
	"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
	"temperature":34.9,
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,23,19,6", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5683",
	"weight":29.9,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"FMRIMeasurement", "date":"2017,5,23,19,6,10", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5683",
	"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
	"irregularities":[
		"Irregularity:description.stimulation apparatus failed, unable to deliver stimulation",
		],
	"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
	"temperature":36.3,
	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,26,10,1", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5699",
	"weight":31.8,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"FMRIMeasurement", "date":"2017,5,26,10,1,12", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5699",
	"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
	"stimulations":[
		"StimulationProtocol:code.chr_longSOA",
		],
	"irregularities":[
		"Irregularity:description.unable to cannulate",
		"Irregularity:description.scanner issues, unable to measure",
		"Irregularity:description.agitated during induction",
		],
	"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
	"temperature":35.4,
	})
# add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,19,13,0", "operator_id":"Operator:code.YPI",
# 	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5699",
# 	"weight":28.0,"weight_unit_id":"MeasurementUnit:code.g"
# 	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,26,13,15", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5692",
	"weight":26.8,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"FMRIMeasurement", "date":"2017,5,26,13,15,50", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5692",
	"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
	"stimulations":[
		"StimulationProtocol:code.chr_longSOA",
		],
	"irregularities":[
		"Irregularity:description.unable to cannulate",
		],
	"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
	"temperature":34.5,
	})
# add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,19,13,0", "operator_id":"Operator:code.YPI",
# 	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5692",
# 	"weight":27.0,"weight_unit_id":"MeasurementUnit:code.g"
# 	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,26,15,21", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5700",
	"weight":38.9,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"FMRIMeasurement", "date":"2017,5,26,15,21,2", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5700",
	"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
	"stimulations":[
		"StimulationProtocol:code.chr_longSOA",
		],
	"irregularities":[
		"Irregularity:description.re-ran adjustments after endorem administration, total delay ~20min",
		],
	"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
	"temperature":36.1,
	})
# add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,19,13,0", "operator_id":"Operator:code.YPI",
# 	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5700",
# 	"weight":27.0,"weight_unit_id":"MeasurementUnit:code.g"
# 	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,26,17,8", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6255",
	"weight":30.7,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"FMRIMeasurement", "date":"2017,5,26,17,8,37", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6255",
	"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
	"stimulations":[
		"StimulationProtocol:code.chr_longSOA",
		],
	"irregularities":[
		'Irregularity:description.died during measurement'
		],
	"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
	"temperature":36.4,
	})
# add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,19,13,0", "operator_id":"Operator:code.YPI",
# 	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6255",
# 	"weight":27.0,"weight_unit_id":"MeasurementUnit:code.g"
# 	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,12,23,38", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5694",
	"weight":26.9,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"FMRIMeasurement", "date":"2017,5,12,23,38,14", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5694",
	"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
	"stimulations":[
		"StimulationProtocol:code.chr_longSOA",
		],
	"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
	"temperature":37.0,
	})
# add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,19,13,0", "operator_id":"Operator:code.YPI",
# 	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5694",
# 	"weight":26.4,"weight_unit_id":"MeasurementUnit:code.g"
# 	})

add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,26,20,12", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6262",
	"weight":24.9,"weight_unit_id":"MeasurementUnit:code.g"
	})
add_generic(db_location, parameters={"CATEGORY":"FMRIMeasurement", "date":"2017,5,26,20,12,10", "operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6262",
	"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
	"stimulations":[
		"StimulationProtocol:code.chr_longSOA",
		],
	"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
	"temperature":34.1,
	"irregularities":[
		"Irregularity:description.scanner issues, unable to measure"
		],
	})
# add_generic(db_location, parameters={"CATEGORY":"WeightMeasurement", "date":"2017,5,19,13,0", "operator_id":"Operator:code.YPI",
# 	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6262",
# 	"weight":24.9,"weight_unit_id":"MeasurementUnit:code.g"
# 	})
