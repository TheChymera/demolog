from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'
add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":13, "reference_date":"2016,5,28,16,40", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2016,5,30,13,0", "sucrose_bottle_position":"left",
	"sucrose_start_amount":308.27,
	"sucrose_end_amount":296.49,
	"water_start_amount":327.37,
	"water_end_amount":322.41})
add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":14, "reference_date":"2016,5,28,16,40", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2016,5,30,13,0", "sucrose_bottle_position":"left",
	"sucrose_start_amount":339.97,
	"sucrose_end_amount":260.98,
	"water_start_amount":359.84,
	"water_end_amount":348.90})
add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":15, "reference_date":"2016,5,28,16,40", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2016,5,30,13,0", "sucrose_bottle_position":"right",
	"sucrose_start_amount":307.97,
	"sucrose_end_amount":290.92,
	"water_start_amount":337.46,
	"water_end_amount":274.82})
add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":16, "reference_date":"2016,5,28,16,40", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2016,5,30,13,0", "sucrose_bottle_position":"right",
	"sucrose_start_amount":294.38,
	"sucrose_end_amount":271.93,
	"water_start_amount":296.33,
	"water_end_amount":287.93})

add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":13, "reference_date":"2016,5,30,13,0", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2016,6,2,16,55", "sucrose_bottle_position":"right",
	"sucrose_start_amount":296.49,
	"sucrose_end_amount":279.51,
	"water_start_amount":322.41,
	"water_end_amount":318.73})
add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":14, "reference_date":"2016,5,30,13,0", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2016,6,2,16,55", "sucrose_bottle_position":"right",
	"sucrose_start_amount":260.98,
	"sucrose_end_amount":191.44,
	"water_start_amount":348.90,
	"water_end_amount":339.78})
add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":15, "reference_date":"2016,5,30,13,0", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2016,6,2,16,55", "sucrose_bottle_position":"left",
	"sucrose_start_amount":290.92,
	"sucrose_end_amount":275.69,
	"water_start_amount":274.82,
	"water_end_amount":204.43})
add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":16, "reference_date":"2016,5,30,13,0", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2016,6,2,16,55", "sucrose_bottle_position":"left",
	"sucrose_start_amount":271.93,
	"sucrose_end_amount":232.82,
	"water_start_amount":287.93,
	"water_end_amount":286.24})

add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":17, "reference_date":"2016,6,21,17,21", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2016,6,23,13,0", "sucrose_bottle_position":"left",
	"sucrose_start_amount":334.41,
	"sucrose_end_amount":291.05,
	"water_start_amount":341.76,
	"water_end_amount":328.17})
add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":18, "reference_date":"2016,6,21,17,21", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2016,6,23,13,0", "sucrose_bottle_position":"left",
	"sucrose_start_amount":346.20,
	"sucrose_end_amount":312.58,
	"water_start_amount":340.51,
	"water_end_amount":329.35})
add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":17, "reference_date":"2016,6,23,13,0", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2016,6,26,16,5", "sucrose_bottle_position":"right",
	"sucrose_start_amount":291.05,
	"sucrose_end_amount":216.54,
	"water_start_amount":328.17,
	"water_end_amount":318.44})
add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":18, "reference_date":"2016,6,23,13,0", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2016,6,26,16,5", "sucrose_bottle_position":"right",
	"sucrose_start_amount":312.58,
	"sucrose_end_amount":257.75,
	"water_start_amount":329.35,
	"water_end_amount":323.49})
