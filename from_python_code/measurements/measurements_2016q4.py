from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'
add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":32, "reference_date":"2016,12,29,23", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2016,12,31,18", "sucrose_bottle_position":"left",
	"sucrose_start_amount":269.2,
	"sucrose_end_amount":257.3,
	"water_start_amount":264.2,
	"water_end_amount":259.1})
add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":33, "reference_date":"2016,12,29,23", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2016,12,31,18", "sucrose_bottle_position":"left",
	"sucrose_start_amount":275.3,
	"sucrose_end_amount":262.9,
	"water_start_amount":282.1,
	"water_end_amount":278.1})
add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":34, "reference_date":"2016,12,29,23", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2016,12,31,18", "sucrose_bottle_position":"left",
	"sucrose_start_amount":285.3,
	"sucrose_end_amount":276.3,
	"water_start_amount":266.4,
	"water_end_amount":261.7})
add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":35, "reference_date":"2016,12,29,23", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2016,12,31,18", "sucrose_bottle_position":"left",
	"sucrose_start_amount":274.8,
	"sucrose_end_amount":266.0,
	"water_start_amount":267.2,
	"water_end_amount":263.2})
add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":36, "reference_date":"2016,12,29,23", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2016,12,31,18", "sucrose_bottle_position":"left",
	"sucrose_start_amount":269.5,
	"sucrose_end_amount":256.1,
	"water_start_amount":261.9,
	"water_end_amount":256.2})
add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":32, "reference_date":"2016,12,31,18", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2017,1,2,15,30", "sucrose_bottle_position":"right",
	"sucrose_start_amount":257.3,
	"sucrose_end_amount":246.0,
	"water_start_amount":259.1,
	"water_end_amount":254.9})
add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":33, "reference_date":"2016,12,31,18", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2017,1,2,15,30", "sucrose_bottle_position":"right",
	"sucrose_start_amount":262.9,
	"sucrose_end_amount":255.7,
	"water_start_amount":278.1,
	"water_end_amount":273.2})
add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":34, "reference_date":"2016,12,31,18", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2017,1,2,15,30", "sucrose_bottle_position":"right",
	"sucrose_start_amount":276.3,
	"sucrose_end_amount":269.2,
	"water_start_amount":260.1,
	"water_end_amount":256.2})
add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":35, "reference_date":"2016,12,31,18", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2017,1,2,15,30", "sucrose_bottle_position":"right",
	"sucrose_start_amount":266.0,
	"sucrose_end_amount":258.4,
	"water_start_amount":263.2,
	"water_end_amount":259.5})
add_generic(db_location, parameters={"CATEGORY":"SucrosePreferenceMeasurement", "cage_id":36, "reference_date":"2016,12,31,18", "concentration_unit_id":"MeasurementUnit:code.percent", "sucrose_concentration":1, "date":"2017,1,2,15,30", "sucrose_bottle_position":"right",
	"sucrose_start_amount":256.1,
	"sucrose_end_amount":248.2,
	"water_start_amount":256.2,
	"water_end_amount":251.9})
