from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'
add_generic(db_location, parameters={"CATEGORY":"OpenFieldTestMeasurement",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5704",
	"date":"2017,3,2,20,47","operator_id":"Operator:code.YPI",
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0079.mkv",
	"center_luminostiy":409,
	"edge_luminostiy":275,
	"arena_id":"Arena:code.aic",
	"evaluations":[{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0071_77-100_chr.csv", "author_id":"Operator:code.YPI"},],})
add_generic(db_location, parameters={"CATEGORY":"OpenFieldTestMeasurement",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5705",
	"date":"2017,3,2,21,21","operator_id":"Operator:code.YPI",
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0080.mkv",
	"center_luminostiy":409,
	"edge_luminostiy":275,
	"arena_id":"Arena:code.aic",
	"evaluations":[{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0071_77-100_chr.csv", "author_id":"Operator:code.YPI"},],})
add_generic(db_location, parameters={"CATEGORY":"OpenFieldTestMeasurement",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5706",
	"date":"2017,3,2,21,56","operator_id":"Operator:code.YPI",
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0081.mkv",
	"center_luminostiy":409,
	"edge_luminostiy":275,
	"arena_id":"Arena:code.aic",
	"evaluations":[{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0071_77-100_chr.csv", "author_id":"Operator:code.YPI"},],})

add_generic(db_location, parameters={"CATEGORY":"OpenFieldTestMeasurement",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5689",
	"date":"2017,3,2,22,32","operator_id":"Operator:code.YPI",
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0082.mkv",
	"center_luminostiy":409,
	"edge_luminostiy":275,
	"arena_id":"Arena:code.aic",
	"evaluations":[{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0071_77-100_chr.csv", "author_id":"Operator:code.YPI"},],})
add_generic(db_location, parameters={"CATEGORY":"OpenFieldTestMeasurement",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5690",
	"date":"2017,3,2,23,0","operator_id":"Operator:code.YPI",
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0083.mkv",
	"center_luminostiy":409,
	"edge_luminostiy":275,
	"arena_id":"Arena:code.aic",
	"evaluations":[{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0071_77-100_chr.csv", "author_id":"Operator:code.YPI"},],})
add_generic(db_location, parameters={"CATEGORY":"OpenFieldTestMeasurement",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5691",
	"date":"2017,3,2,23,33","operator_id":"Operator:code.YPI",
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0084.mkv",
	"center_luminostiy":409,
	"edge_luminostiy":275,
	"arena_id":"Arena:code.aic",
	"evaluations":[{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0071_77-100_chr.csv", "author_id":"Operator:code.YPI"},],})


add_generic(db_location, parameters={"CATEGORY":"ForcedSwimTestMeasurement", "date":"2017,3,3,20,15","operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5704",
	"temperature":22.8,
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0085.mkv",
	"recording_bracket":"0-33",
	"evaluations":[
		{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0085_0-33_chr.csv", "author_id":"Operator:code.YPI"},
		],})
add_generic(db_location, parameters={"CATEGORY":"ForcedSwimTestMeasurement", "date":"2017,3,3,20,15","operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5705",
	"temperature":22.8,
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0085.mkv",
	"recording_bracket":"34-63",
	"evaluations":[
		{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0085_34-63_chr.csv", "author_id":"Operator:code.YPI"},
		],})
add_generic(db_location, parameters={"CATEGORY":"ForcedSwimTestMeasurement", "date":"2017,3,3,20,15","operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5706",
	"temperature":22.8,
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0085.mkv",
	"recording_bracket":"64-96",
	"evaluations":[
		{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0085_64-96_chr.csv", "author_id":"Operator:code.YPI"},
		],})

add_generic(db_location, parameters={"CATEGORY":"ForcedSwimTestMeasurement", "date":"2017,3,3,20,45","operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5689",
	"temperature":22.8,
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0086.mkv",
	"recording_bracket":"0-32",
	"evaluations":[
		{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0086_0-32_chr.csv", "author_id":"Operator:code.YPI"},
		],})
add_generic(db_location, parameters={"CATEGORY":"ForcedSwimTestMeasurement", "date":"2017,3,3,20,45","operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5690",
	"temperature":22.8,
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0086.mkv",
	"recording_bracket":"33-64",
	"evaluations":[
		{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0086_33-64_chr.csv", "author_id":"Operator:code.YPI"},
		],})
add_generic(db_location, parameters={"CATEGORY":"ForcedSwimTestMeasurement", "date":"2017,3,3,20,45","operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5691",
	"temperature":22.8,
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0086.mkv",
	"recording_bracket":"65-98",
	"evaluations":[
		{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0086_65-98_chr.csv", "author_id":"Operator:code.YPI"},
		],})
