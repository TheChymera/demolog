from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'
add_generic(db_location, parameters={"CATEGORY":"OpenFieldTestMeasurement",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5673",
	"date":"2016,12,28,19,57","operator_id":"Operator:code.YPI",
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0072.mkv",
	"center_luminostiy":412,
	"edge_luminostiy":285,
	"arena_id":"Arena:code.aic",
	"evaluations":[{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0071_77-100_chr.csv", "author_id":"Operator:code.YPI"},],})
add_generic(db_location, parameters={"CATEGORY":"OpenFieldTestMeasurement",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5674",
	"date":"2016,12,28,20,28","operator_id":"Operator:code.YPI",
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0073.mkv",
	"center_luminostiy":412,
	"edge_luminostiy":285,
	"arena_id":"Arena:code.aic",
	"evaluations":[{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0071_77-100_chr.csv", "author_id":"Operator:code.YPI"},],})
add_generic(db_location, parameters={"CATEGORY":"OpenFieldTestMeasurement",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5675",
	"date":"2016,12,28,21,06","operator_id":"Operator:code.YPI",
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0074.mkv",
	"center_luminostiy":412,
	"edge_luminostiy":285,
	"arena_id":"Arena:code.aic",
	"evaluations":[{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0071_77-100_chr.csv", "author_id":"Operator:code.YPI"},],})
add_generic(db_location, parameters={"CATEGORY":"OpenFieldTestMeasurement",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5667",
	"date":"2016,12,28,21,43","operator_id":"Operator:code.YPI",
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0075.mkv",
	"center_luminostiy":412,
	"edge_luminostiy":285,
	"arena_id":"Arena:code.aic",
	"evaluations":[{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0071_77-100_chr.csv", "author_id":"Operator:code.YPI"},],})
add_generic(db_location, parameters={"CATEGORY":"OpenFieldTestMeasurement",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5668",
	"date":"2016,12,28,22,16","operator_id":"Operator:code.YPI",
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0076.mkv",
	"center_luminostiy":412,
	"edge_luminostiy":285,
	"arena_id":"Arena:code.aic",
	"evaluations":[{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0071_77-100_chr.csv", "author_id":"Operator:code.YPI"},],})

add_generic(db_location, parameters={"CATEGORY":"ForcedSwimTestMeasurement", "date":"2016,12,29,21,24","operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5667",
	"temperature":22.8,
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0077.mkv",
	"recording_bracket":"0-50",
	"evaluations":[
		{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0077_0-50_chr.csv", "author_id":"Operator:code.YPI"},
		],})
add_generic(db_location, parameters={"CATEGORY":"ForcedSwimTestMeasurement", "date":"2016,12,29,21,24","operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5668",
	"temperature":22.8,
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0077.mkv",
	"recording_bracket":"50-100",
	"evaluations":[
		{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0077_50-100_chr.csv", "author_id":"Operator:code.YPI"},
		],})
add_generic(db_location, parameters={"CATEGORY":"ForcedSwimTestMeasurement", "date":"2016,12,29,21,47","operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5673",
	"temperature":22.8,
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0078.mkv",
	"recording_bracket":"0-33",
	"evaluations":[
		{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0078_0-33_chr.csv", "author_id":"Operator:code.YPI"},
		],})
add_generic(db_location, parameters={"CATEGORY":"ForcedSwimTestMeasurement", "date":"2016,12,29,21,47","operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5674",
	"temperature":22.8,
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0078.mkv",
	"recording_bracket":"34-66",
	"evaluations":[
		{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0078_34-66_chr.csv", "author_id":"Operator:code.YPI"},
		],})
add_generic(db_location, parameters={"CATEGORY":"ForcedSwimTestMeasurement", "date":"2016,12,29,21,47","operator_id":"Operator:code.YPI",
	"animal_id":"Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5675",
	"temperature":22.8,
	"data_path":"~/src/demolog/data/cameras/nd750/a/nd750_a0078.mkv",
	"recording_bracket":"67-100",
	"evaluations":[
		{"CATEGORY":"Evaluation", "path":"~/src/demolog/data/behaviour/forced_swim_test/nd750_a0078_67-100_chr.csv", "author_id":"Operator:code.YPI"},
		],})
