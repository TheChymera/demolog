from labbookdb.db.add import add_generic
db_location = 'meta.db'
#measurement units

add_generic(db_location, parameters={"CATEGORY":"MeasurementUnit", "code":"rpm", "long_name":"rotations per minute", "siunitx":"\\rpm"}) # This requires manual secificatioan in latex document preamble `\DeclareSIUnit\rpm{rpm}`
add_generic(db_location, parameters={"CATEGORY":"MeasurementUnit", "code":"s", "long_name":"second", "siunitx":"\\second"})
add_generic(db_location, parameters={"CATEGORY":"MeasurementUnit", "code":"dC", "long_name":"degrees Celsius", "siunitx":"\\celsius"})
add_generic(db_location, parameters={"CATEGORY":"MeasurementUnit", "code":"g", "long_name":"gram", "siunitx":"\\gram"})
add_generic(db_location, parameters={"CATEGORY":"MeasurementUnit", "code":"Hz", "long_name":"hertz", "siunitx":"\\hertz"})
add_generic(db_location, parameters={"CATEGORY":"MeasurementUnit", "code":"%", "long_name":"percent", "siunitx":"\\percent"})
add_generic(db_location, parameters={"CATEGORY":"MeasurementUnit", "code":"mul", "long_name":"microlitre", "siunitx":"\\micro\\litre"})
add_generic(db_location, parameters={"CATEGORY":"MeasurementUnit", "code":"min", "long_name":"minute", "siunitx":"\\minute"})
add_generic(db_location, parameters={"CATEGORY":"MeasurementUnit", "code":"mg/l", "long_name":"milligram per litre", "siunitx":"\\milli\\gram\\per\\litre"})
add_generic(db_location, parameters={"CATEGORY":"MeasurementUnit", "code":"g/l", "long_name":"gram per litre", "siunitx":"\\gram\\per\\litre"})
add_generic(db_location, parameters={"CATEGORY":"MeasurementUnit", "code":"mg/ml", "long_name":"milligram per millilitre", "siunitx":"\\milli\\gram\\per\\milli\\litre"})
add_generic(db_location, parameters={"CATEGORY":"MeasurementUnit", "code":"mul/g", "long_name":"microlitre per gram", "siunitx":"\\micro\\litre\\per\\gram"})
add_generic(db_location, parameters={"CATEGORY":"MeasurementUnit", "code":"mul/s", "long_name":"microlitre per second", "siunitx":"\\micro\\litre\\per\\second"})
add_generic(db_location, parameters={"CATEGORY":"MeasurementUnit", "code":"mul/g/h", "long_name":"microlitre per second", "siunitx":"\\micro\\litre\\per\\gram\\per\\hour"})
add_generic(db_location, parameters={"CATEGORY":"MeasurementUnit", "code":"mM", "long_name":"millimolar", "siunitx":"\\milli\\Molar"}) # This requires manual secificatioan in latex document preamble
add_generic(db_location, parameters={"CATEGORY":"MeasurementUnit", "code":"percent", "long_name":"percent", "siunitx":"\\percent"})
add_generic(db_location, parameters={"CATEGORY":"MeasurementUnit", "code":"mm", "long_name":"millimetre", "siunitx":"\\milli\\metre"})
add_generic(db_location, parameters={"CATEGORY":"MeasurementUnit", "code":"mW", "long_name":"milliwatt", "siunitx":"\\milli\\Watt"})
add_generic(db_location, parameters={"CATEGORY":"MeasurementUnit", "code":"mA", "long_name":"milliampere", "siunitx":"\\milli\\Ampere"})
