from labbookdb.db.add import add_generic
db_location = 'meta.db'
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":17, "date":"2016,4,15,19", "reference_date":"2016,4,11,14", "start_amount":348.92, "end_amount":277.50})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":18, "date":"2016,4,15,19", "reference_date":"2016,4,11,14", "start_amount":368.08, "end_amount":300.05})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":17, "date":"2016,4,21,12,30", "reference_date":"2016,4,15,19", "start_amount":344.75, "end_amount":247.23})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":18, "date":"2016,4,21,12,30", "reference_date":"2016,4,15,19", "start_amount":355.45, "end_amount":265.16})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":17, "reference_date":"2016,4,21,12,30", "start_amount":247.23,
"date":"2016,4,28,14,45", "end_amount":129.24})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":18, "reference_date":"2016,4,21,12,30", "start_amount":265.16,
"date":"2016,4,28,14,45", "end_amount":144.53})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":17, "reference_date":"2016,4,28,14,45", "start_amount":379.31,
"date":"2016,5,5,13,30", "end_amount":258.67})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":18, "reference_date":"2016,4,28,14,45", "start_amount":375.85,
"date":"2016,5,5,13,30", "end_amount":263.86})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":17, "reference_date":"2016,5,5,14,45", "start_amount":379.31,
"date":"2016,5,12,17,25", "end_amount":204.10})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":18, "reference_date":"2016,5,5,14,45", "start_amount":375.85,
"date":"2016,5,12,17,25", "end_amount":230.74})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":17, "reference_date":"2016,5,12,17,30", "start_amount":325.74,
"date":"2016,5,19,15,20", "end_amount":198.86})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":18, "reference_date":"2016,5,12,17,30", "start_amount":362.54,
"date":"2016,5,19,15,20", "end_amount":234.82})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":17, "reference_date":"2016,5,19,23,05", "start_amount":322.6,
"date":"2016,5,27,17,5", "end_amount":205.44})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":18, "reference_date":"2016,5,19,23,05", "start_amount":350.51,
"date":"2016,5,27,17,5", "end_amount":224.3})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":17, "reference_date":"2016,5,27,17,5", "start_amount":323.58,
"date":"2016,6,2,14,30", "end_amount":230.68})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":18, "reference_date":"2016,5,27,17,5", "start_amount":330.96,
"date":"2016,6,2,14,30", "end_amount":228.94})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":17, "reference_date":"2016,6,2,16,45", "start_amount":341.24,
"date":"2016,6,9,18,35", "end_amount":246.37})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":18, "reference_date":"2016,6,2,16,45", "start_amount":356.1,
"date":"2016,6,9,18,35", "end_amount":242.72})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":17, "reference_date":"2016,6,9,18,40", "start_amount":335.36,
"date":"2016,6,16,13,31", "end_amount":221.79})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":18, "reference_date":"2016,6,9,18,40", "start_amount":355.22,
"date":"2016,6,16,13,31", "end_amount":243.35})

add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":13, "reference_date":"2016,4,11,17", "start_amount":365.93,
"date":"2016,4,20,16", "end_amount":329.07})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":14, "reference_date":"2016,4,11,17", "start_amount":375.5,
"date":"2016,4,20,16", "end_amount":227.12})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":15, "reference_date":"2016,4,11,17", "start_amount":354.72,
"date":"2016,4,20,16", "end_amount":291.83})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":16, "reference_date":"2016,4,11,17", "start_amount":364.97,
"date":"2016,4,20,16", "end_amount":238.39})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":13, "reference_date":"2016,4,20,16", "start_amount":347.26,
"date":"2016,4,25,11", "end_amount":320.75})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":14, "reference_date":"2016,4,20,16", "start_amount":359.66,
"date":"2016,4,25,11", "end_amount":276.54})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":15, "reference_date":"2016,4,20,16", "start_amount":370.74,
"date":"2016,4,25,11", "end_amount":335.02})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":16, "reference_date":"2016,4,20,16", "start_amount":369.28,
"date":"2016,4,25,11", "end_amount":306.08})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":13, "reference_date":"2016,4,25,19,30", "start_amount":343.76,
"date":"2016,5,2,10,50", "end_amount":311.76})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":14, "reference_date":"2016,4,25,19,30", "start_amount":332,
"date":"2016,5,2,10,50", "end_amount":251.48})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":15, "reference_date":"2016,4,25,19,30", "start_amount":339.71,
"date":"2016,5,2,10,50", "end_amount":288.36})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":16, "reference_date":"2016,4,25,19,30", "start_amount":356.5,
"date":"2016,5,2,10,50", "end_amount":282.80})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":13, "reference_date":"2016,5,2,10,50", "start_amount":369.8,
"date":"2016,5,9,10,40", "end_amount":343.07})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":14, "reference_date":"2016,5,2,10,50", "start_amount":359,
"date":"2016,5,9,10,40", "end_amount":274.88})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":15, "reference_date":"2016,5,2,10,50", "start_amount":359.8,
"date":"2016,5,9,10,40", "end_amount":307.56})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":16, "reference_date":"2016,5,2,10,50", "start_amount":372.44,
"date":"2016,5,9,10,40", "end_amount":316.78})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":13, "reference_date":"2016,5,9,17,35", "start_amount":351.24,
"date":"2016,5,16,16,30", "end_amount":316.72})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":14, "reference_date":"2016,5,9,17,35", "start_amount":365.82,
"date":"2016,5,16,16,30", "end_amount":278.53})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":15, "reference_date":"2016,5,9,17,35", "start_amount":349,
"date":"2016,5,16,16,30", "end_amount":297.18})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":16, "reference_date":"2016,5,9,17,35", "start_amount":339.61,
"date":"2016,5,16,16,30", "end_amount":278.34})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":13, "reference_date":"2016,5,16,16,35", "start_amount":343.24,
"date":"2016,5,23,11,0", "end_amount":313.05})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":14, "reference_date":"2016,5,16,16,35", "start_amount":352.64,
"date":"2016,5,23,11,0", "end_amount":261.81})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":15, "reference_date":"2016,5,16,16,35", "start_amount":339.22,
"date":"2016,5,23,11,0", "end_amount":285.41})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":16, "reference_date":"2016,5,16,16,35", "start_amount":367.64,
"date":"2016,5,23,11,0", "end_amount":307.73})
