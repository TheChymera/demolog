from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'

add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":25, "reference_date":"2017,1,4,16,30", "start_amount":369.6,
"date":"2017,1,10,16,30", "end_amount":288.9})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":27, "reference_date":"2017,1,4,16,30", "start_amount":366.1,
"date":"2017,1,10,16,30", "end_amount":300.5})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":29, "reference_date":"2017,1,2,15,30", "start_amount":349.9,
"date":"2017,1,10,16,30", "end_amount":246.4, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":29, "reference_date":"2017,1,10,16,30", "start_amount":335.9,
"date":"2017,1,16,15", "end_amount":262.7, "operator_id":"Operator:code.YPI"})

add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":37, "reference_date":"2017,1,16,23,50", "start_amount":351.6,
"date":"2017,1,24,18", "end_amount":322.6, "operator_id":"Operator:code.YPI"})

add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":38, "reference_date":"2017,1,17,23,0", "start_amount":372.0,
"date":"2017,1,24,18", "end_amount":293.8, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":38, "reference_date":"2017,1,24,18", "start_amount":293.8,
"date":"2017,1,31,11", "end_amount":217.5, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":38, "reference_date":"2017,1,31,11", "start_amount":388.1,
"date":"2017,2,8,13", "end_amount":304.3, "operator_id":"Operator:code.YoC"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":38, "reference_date":"2017,2,8,13", "start_amount":304.3,
"date":"2017,2,14,16,30", "end_amount":247.5, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":38, "reference_date":"2017,2,14,22", "start_amount":386.0,
"date":"2017,2,21,16,30", "end_amount":318.2, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":38, "reference_date":"2017,2,21,16,30", "start_amount":318.2,
"date":"2017,2,28,12,0", "end_amount":249.9, "operator_id":"Operator:code.YPI"})

add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":39, "reference_date":"2017,1,17,23,0", "start_amount":346.8,
"date":"2017,1,24,18", "end_amount":239.5, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":39, "reference_date":"2017,1,24,18", "start_amount":239.5,
"date":"2017,1,31,11", "end_amount":140.6, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":39, "reference_date":"2017,1,31,22", "start_amount":391.5,
"date":"2017,2,8,13", "end_amount":287.9, "operator_id":"Operator:code.YoC"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":39, "reference_date":"2017,2,8,13", "start_amount":287.9,
"date":"2017,2,14,16,30", "end_amount":214.7, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":39, "reference_date":"2017,2,14,22", "start_amount":387.7,
"date":"2017,2,21,16,30", "end_amount":301.7, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":39, "reference_date":"2017,2,21,16,30", "start_amount":301.7,
"date":"2017,2,28,12,0", "end_amount":211.1, "operator_id":"Operator:code.YPI"})

add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":40, "reference_date":"2017,1,17,23,0", "start_amount":331.2,
"date":"2017,1,24,18", "end_amount":251.5, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":40, "reference_date":"2017,1,24,18", "start_amount":251.5,
"date":"2017,1,31,11", "end_amount":166.3, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":40, "reference_date":"2017,1,31,22", "start_amount":388.7,
"date":"2017,2,8,13", "end_amount":306.9, "operator_id":"Operator:code.YoC"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":40, "reference_date":"2017,2,8,13", "start_amount":306.9,
"date":"2017,2,14,14", "end_amount":274.7, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":40, "reference_date":"2017,2,14,23,55", "start_amount":382.0,
"date":"2017,2,21,16,30", "end_amount":311.7, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":40, "reference_date":"2017,2,21,16,30", "start_amount":311.7,
"date":"2017,2,28,12,0", "end_amount":235.3, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":40, "reference_date":"2017,3,13,17,0", "start_amount":352.8,
"date":"2017,3,17,13,40", "end_amount":303.1, "operator_id":"Operator:code.YPI"})

add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":41, "reference_date":"2017,1,17,23,0", "start_amount":349.6,
"date":"2017,1,24,18", "end_amount":281.6, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":41, "reference_date":"2017,1,24,18", "start_amount":281.6,
"date":"2017,1,31,11", "end_amount":195.8, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":41, "reference_date":"2017,1,31,22", "start_amount":393.4,
"date":"2017,2,8,13", "end_amount":337.1, "operator_id":"Operator:code.YoC"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":41, "reference_date":"2017,2,8,13", "start_amount":337.1,
"date":"2017,2,14,14", "end_amount":241.6, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":41, "reference_date":"2017,2,14,23,55", "start_amount":383.0,
"date":"2017,2,21,16,30", "end_amount":321.2, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":41, "reference_date":"2017,2,21,16,30", "start_amount":321.2,
"date":"2017,2,28,12,0", "end_amount":244.9, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":41, "reference_date":"2017,3,13,17,0", "start_amount":359.0,
"date":"2017,3,17,13,40", "end_amount":310.5, "operator_id":"Operator:code.YPI"})

add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":42, "reference_date":"2017,1,17,23,0", "start_amount":363.1,
"date":"2017,1,24,18", "end_amount":285.9, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":42, "reference_date":"2017,1,24,18", "start_amount":285.9,
"date":"2017,1,31,11", "end_amount":199.2, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":42, "reference_date":"2017,1,31,11", "start_amount":386.0,
"date":"2017,2,8,13", "end_amount":300.3, "operator_id":"Operator:code.YoC"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":42, "reference_date":"2017,2,8,13", "start_amount":300.3,
"date":"2017,2,14,16,30", "end_amount":236.8, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":42, "reference_date":"2017,2,14,22", "start_amount":387.7,
"date":"2017,2,21,16,30", "end_amount":315.9, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":42, "reference_date":"2017,2,21,16,30", "start_amount":315.9,
"date":"2017,2,28,12,0", "end_amount":287.1, "operator_id":"Operator:code.YPI"})

add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":44, "reference_date":"2017,2,21,16,30", "start_amount":366.0,
"date":"2017,2,28,12,0", "end_amount":328.5, "operator_id":"Operator:code.YPI"})

add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":45, "reference_date":"2017,2,21,16,30", "start_amount":357.1,
"date":"2017,2,28,12,0", "end_amount":331.3, "operator_id":"Operator:code.YPI"})
