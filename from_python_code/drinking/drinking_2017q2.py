from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'

add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":38, "reference_date":"2017,5,13,19", "start_amount":333.4,
"date":"2017,5,19,13", "end_amount":278.4, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":46, "reference_date":"2017,5,13,19", "start_amount":360.9,
"date":"2017,5,19,13", "end_amount":289.4, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":42, "reference_date":"2017,5,13,19", "start_amount":341.4,
"date":"2017,5,19,13", "end_amount":313.8, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":45, "reference_date":"2017,5,13,19", "start_amount":342.8,
"date":"2017,5,19,13", "end_amount":315.0, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":57, "reference_date":"2017,5,13,19", "start_amount":340.3,
"date":"2017,5,19,13", "end_amount":314.4, "operator_id":"Operator:code.YPI"})

add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":38, "reference_date":"2017,5,19,13", "start_amount":278.4,
"date":"2017,5,26,9", "end_amount":202.9, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":46, "reference_date":"2017,5,19,13", "start_amount":289.4,
"date":"2017,5,26,9", "end_amount":200.5, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":42, "reference_date":"2017,5,19,13", "start_amount":313.8,
"date":"2017,5,26,9", "end_amount":283.6, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":45, "reference_date":"2017,5,19,13", "start_amount":315.0,
"date":"2017,5,26,9", "end_amount":279.5, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":57, "reference_date":"2017,5,19,13", "start_amount":314.4,
"date":"2017,5,26,9", "end_amount":279.0, "operator_id":"Operator:code.YPI"})
#
# add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":38, "reference_date":"2017,5,22,0", "start_amount":278.4,
# "date":"2017,5,26,9", "end_amount":202.9, "operator_id":"Operator:code.YPI"})
# add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":46, "reference_date":"2017,5,22,0", "start_amount":289.4,
# "date":"2017,5,26,9", "end_amount":200.5, "operator_id":"Operator:code.YPI"})
# add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":42, "reference_date":"2017,5,22,0", "start_amount":313.8,
# "date":"2017,5,26,9", "end_amount":283.6, "operator_id":"Operator:code.YPI"})
# add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":45, "reference_date":"2017,5,22,0", "start_amount":315.0,
# "date":"2017,5,26,9", "end_amount":279.5, "operator_id":"Operator:code.YPI"})
# add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":57, "reference_date":"2017,5,22,0", "start_amount":314.4,
# "date":"2017,5,26,9", "end_amount":279.0, "operator_id":"Operator:code.YPI"})
