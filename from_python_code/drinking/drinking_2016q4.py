from labbookdb.db.add import add_generic
db_location = 'meta.db'
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":25, "reference_date":"2016,10,20,18", "start_amount":350.8,
"date":"2016,11,4,16", "end_amount":262.2})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":26, "reference_date":"2016,10,20,18", "start_amount":345.7,
"date":"2016,11,4,16", "end_amount":244.3})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":27, "reference_date":"2016,10,20,18", "start_amount":343.2,
"date":"2016,11,4,16", "end_amount":255.1})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":28, "reference_date":"2016,10,20,18", "start_amount":314.8,
"date":"2016,11,4,16", "end_amount":279.2})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":25, "reference_date":"2016,10,28,4", "start_amount":376.5,
"date":"2016,11,4,17", "end_amount":287.9})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":26, "reference_date":"2016,10,28,4", "start_amount":363.5,
"date":"2016,11,4,17", "end_amount":275.6})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":27, "reference_date":"2016,10,28,4", "start_amount":355,
"date":"2016,11,4,17", "end_amount":230.7})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":28, "reference_date":"2016,10,28,4", "start_amount":327.8,
"date":"2016,11,4,17", "end_amount":291.0})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":25, "reference_date":"2016,11,4,17,30", "start_amount":360.5,
"date":"2016,11,10,11", "end_amount":288.9})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":26, "reference_date":"2016,11,4,17,30", "start_amount":364.7,
"date":"2016,11,10,11", "end_amount":297.8})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":27, "reference_date":"2016,11,4,17,30", "start_amount":368.3,
"date":"2016,11,10,11", "end_amount":284.4})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":28, "reference_date":"2016,11,4,17,30", "start_amount":371.3,
"date":"2016,11,10,11", "end_amount":333.9})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":25, "reference_date":"2016,11,10,23,0", "start_amount":367.8,
"date":"2016,11,17,14", "end_amount":295.7})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":27, "reference_date":"2016,11,10,23,0", "start_amount":367.5,
"date":"2016,11,17,14", "end_amount":279.2})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":28, "reference_date":"2016,11,10,23,0", "start_amount":343.6,
"date":"2016,11,17,14", "end_amount":209.3})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":25, "reference_date":"2016,11,17,14", "start_amount":371.4,
"date":"2016,11,24,12,30", "end_amount":301.7})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":27, "reference_date":"2016,11,17,14", "start_amount":366.8,
"date":"2016,11,24,12,30", "end_amount":271.4})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":28, "reference_date":"2016,11,17,14", "start_amount":369.2,
"date":"2016,11,24,12,30", "end_amount":338.8})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":25, "reference_date":"2016,11,24,21,30", "start_amount":372.3,
"date":"2016,12,1,13,30", "end_amount":337.1})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":27, "reference_date":"2016,11,24,21,30", "start_amount":379.8,
"date":"2016,12,1,13,30", "end_amount":312.6})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":25, "reference_date":"2016,12,1,14,0", "start_amount":337.1,
"date":"2016,12,8,13,30", "end_amount":242.2})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":27, "reference_date":"2016,12,1,14,0", "start_amount":312.6,
"date":"2016,12,8,13,30", "end_amount":242.5})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":25, "reference_date":"2016,12,8,17,58", "start_amount":338.5,
"date":"2016,12,14,20,30", "end_amount":299.9})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":27, "reference_date":"2016,12,8,17,58", "start_amount":364.9,
"date":"2016,12,14,20,30", "end_amount":308.2})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":25, "reference_date":"2016,12,14,20,30", "start_amount":299.9,
"date":"2016,12,22,13,0", "end_amount":249.1})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":27, "reference_date":"2016,12,14,20,30", "start_amount":308.2,
"date":"2016,12,22,13,0", "end_amount":233.2})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":29, "reference_date":"2016,12,14,20,0", "start_amount":364.6,
"date":"2016,12,22,13,0", "end_amount":261.8, "operator_id":"Operator:code.YPI"})
add_generic(db_location, parameters={"CATEGORY":"DrinkingMeasurement", "cage_id":29, "reference_date":"2016,12,22,13,0", "start_amount":355.8,
"date":"2017,1,2,15,30", "end_amount":227.51, "operator_id":"Operator:code.YPI"})
