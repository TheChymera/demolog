from labbookdb.db.add import append_parameter, add_generic
db_location = 'meta.db'
#create treatments
add_generic(db_location, parameters={"CATEGORY":"Treatment", "start_date":"2017,5,26,19,0", "end_date":"2017,12,26", "protocol_id":"TreatmentProtocol:code.cFluDW"})
add_generic(db_location, parameters={"CATEGORY":"Treatment", "start_date":"2017,5,26,19,0", "end_date":"2017,12,26", "protocol_id":"TreatmentProtocol:code.cFluDW_"})


#add cage treatments
append_parameter(db_location, entry_identification="Cage:id.38",parameters={"treatments":["Treatment:start_date.2017,5,26,19,0&&protocol_id.TreatmentProtocol:code.cFluDW"]})
append_parameter(db_location, entry_identification="Cage:id.46",parameters={"treatments":["Treatment:start_date.2017,5,26,19,0&&protocol_id.TreatmentProtocol:code.cFluDW_"]})
append_parameter(db_location, entry_identification="Cage:id.42",parameters={"treatments":["Treatment:start_date.2017,5,26,19,0&&protocol_id.TreatmentProtocol:code.cFluDW_"]})
append_parameter(db_location, entry_identification="Cage:id.45",parameters={"treatments":["Treatment:start_date.2017,5,26,19,0&&protocol_id.TreatmentProtocol:code.cFluDW_"]})
append_parameter(db_location, entry_identification="Cage:id.57",parameters={"treatments":["Treatment:start_date.2017,5,26,19,0&&protocol_id.TreatmentProtocol:code.cFluDW_"]})

#add animal treatments
append_parameter(db_location,
	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5692",
	parameters={"treatments":[{"CATEGORY":"Treatment","start_date":"2017,5,26,14,34", "protocol_id":"TreatmentProtocol:code.aFluIV_"}]}
	)
append_parameter(db_location,
	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5699",
	parameters={"treatments":[{"CATEGORY":"Treatment","start_date":"2017,5,26,16,45", "protocol_id":"TreatmentProtocol:code.aFluIP"}]}
	)
append_parameter(db_location,
	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5700",
	parameters={"treatments":[{"CATEGORY":"Treatment","start_date":"2017,5,26,16,0", "protocol_id":"TreatmentProtocol:code.aFluIV"}]}
	)
append_parameter(db_location,
	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6255",
	parameters={"treatments":[{"CATEGORY":"Treatment","start_date":"2017,5,26,18,13", "protocol_id":"TreatmentProtocol:code.aFluIV_"}]}
	)
append_parameter(db_location,
	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5694",
	parameters={"treatments":[{"CATEGORY":"Treatment","start_date":"2017,5,26,19,16", "protocol_id":"TreatmentProtocol:code.aFluIV_"}]}
	)
append_parameter(db_location,
	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6262",
	parameters={"treatments":[{"CATEGORY":"Treatment","start_date":"2017,5,26,20,30", "protocol_id":"TreatmentProtocol:code.aFluIV_"}]}
	)
# append_parameter(db_location,
# 	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5692",
# 	parameters={"treatments":["Treatment:start_date.2017,5,26,14,34&&protocol_id.TreatmentProtocol:code.cFluIV_"]}
# 	)
# append_parameter(db_location,
# 	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5699",
# 	parameters={"treatments":["Treatment:start_date.2017,5,26,16,45&&protocol_id.TreatmentProtocol:code.cFluIP"]}
# 	)
# append_parameter(db_location,
# 	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5700",
# 	parameters={"treatments":["Treatment:start_date.2017,5,26,16,0&&protocol_id.TreatmentProtocol:code.aFluIV"]}
# 	)
# append_parameter(db_location,
# 	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6255",
# 	parameters={"treatments":["Treatment:start_date.2017,5,26,16,13&&protocol_id.TreatmentProtocol:code.aFluIV_"]}
# 	)
# append_parameter(db_location,
# 	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5694",
# 	parameters={"treatments":["Treatment:start_date.2017,5,26,19,16&&protocol_id.TreatmentProtocol:code.aFluIV_"]}
# 	)
# append_parameter(db_location,
# 	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6262",
# 	parameters={"treatments":["Treatment:start_date.2017,5,26,20,30&&protocol_id.TreatmentProtocol:code.aFluIV_"]}
# 	)
