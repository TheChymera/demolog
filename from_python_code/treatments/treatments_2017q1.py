from labbookdb.db.add import append_parameter, add_generic
db_location = 'meta.db'
#create treatments
add_generic(db_location, parameters={"CATEGORY":"Treatment", "start_date":"2017,1,31,22,0", "end_date":"2017,3,3,20", "protocol_id":"TreatmentProtocol:code.cFluDW"})
add_generic(db_location, parameters={"CATEGORY":"Treatment", "start_date":"2017,1,31,22,0", "end_date":"2017,3,3,20", "protocol_id":"TreatmentProtocol:code.cFluDW_"})
add_generic(db_location, parameters={"CATEGORY":"Treatment", "start_date":"2017,1,31", "protocol_id":"TreatmentProtocol:code.aFluIV"})
add_generic(db_location, parameters={"CATEGORY":"Treatment", "start_date":"2017,1,31", "protocol_id":"TreatmentProtocol:code.aFluIV_"})

#add cage treatments
append_parameter(db_location, entry_identification="Cage:id.40",parameters={"treatments":["Treatment:start_date.2017,1,31,22,0&&protocol_id.TreatmentProtocol:code.cFluDW_"]})
append_parameter(db_location, entry_identification="Cage:id.41",parameters={"treatments":["Treatment:start_date.2017,1,31,22,0&&protocol_id.TreatmentProtocol:code.cFluDW"]})

#add animal treatments
append_parameter(db_location,
	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5704",
	parameters={"treatments":["Treatment:start_date.2017,1,31&&protocol_id.TreatmentProtocol:code.aFluIV_"]}
	)
append_parameter(db_location,
	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5705",
	parameters={"treatments":["Treatment:start_date.2017,1,31&&protocol_id.TreatmentProtocol:code.aFluIV_"]}
	)
append_parameter(db_location,
	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5706",
	parameters={"treatments":["Treatment:start_date.2017,1,31&&protocol_id.TreatmentProtocol:code.aFluIV_"]}
	)

append_parameter(db_location,
	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5689",
	parameters={"treatments":["Treatment:start_date.2017,1,31&&protocol_id.TreatmentProtocol:code.aFluIV"]}
	)
append_parameter(db_location,
	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5690",
	parameters={"treatments":["Treatment:start_date.2017,1,31&&protocol_id.TreatmentProtocol:code.aFluIV"]}
	)
append_parameter(db_location,
	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5691",
	parameters={"treatments":["Treatment:start_date.2017,1,31&&protocol_id.TreatmentProtocol:code.aFluIV_"]}
	)
