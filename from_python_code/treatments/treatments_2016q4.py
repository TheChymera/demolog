from labbookdb.db.add import append_parameter, add_generic
db_location = 'meta.db'
#create treatments
add_generic(db_location, parameters={"CATEGORY":"Treatment", "start_date":"2016,11,24,21,30", "end_date":"2016,12,26", "protocol_id":"TreatmentProtocol:code.cFluDW"})
add_generic(db_location, parameters={"CATEGORY":"Treatment", "start_date":"2016,11,24,21,30", "end_date":"2016,12,26", "protocol_id":"TreatmentProtocol:code.cFluDW_"})


#add cage treatments
append_parameter(db_location, entry_identification="Cage:id.25",parameters={"treatments":["Treatment:start_date.2016,11,24,21,30&&protocol_id.TreatmentProtocol:code.cFluDW"]})
append_parameter(db_location, entry_identification="Cage:id.27",parameters={"treatments":["Treatment:start_date.2016,11,24,21,30&&protocol_id.TreatmentProtocol:code.cFluDW_"]})

#add animal treatments
append_parameter(db_location,
	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5667",
	parameters={"treatments":[{"CATEGORY":"Treatment", "start_date":"2016,11,24,21,5", "protocol_id":"TreatmentProtocol:code.aFluIV"}]})
append_parameter(db_location,
	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5668",
	parameters={"treatments":[{"CATEGORY":"Treatment", "start_date":"2016,11,24,20,12", "protocol_id":"TreatmentProtocol:code.aFluIV"}]})
append_parameter(db_location,
	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5672",
	parameters={"treatments":[{"CATEGORY":"Treatment", "start_date":"2016,11,24,15,45", "protocol_id":"TreatmentProtocol:code.aFluIV"}]})
append_parameter(db_location,
	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5673",
	parameters={"treatments":[{"CATEGORY":"Treatment", "start_date":"2016,11,24,17,56", "protocol_id":"TreatmentProtocol:code.aFluSC"}]})
append_parameter(db_location,
	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5674",
	parameters={"treatments":[{"CATEGORY":"Treatment", "start_date":"2016,11,24,18,37", "protocol_id":"TreatmentProtocol:code.aFluIV"}]})
append_parameter(db_location,
	entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5675",
	parameters={"treatments":[{"CATEGORY":"Treatment", "start_date":"2016,11,24,19,22", "protocol_id":"TreatmentProtocol:code.aFluIV"}]})
