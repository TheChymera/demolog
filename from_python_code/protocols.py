from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'
#Arenas
add_generic(db_location, parameters={"CATEGORY":"Arena", "code":"aic",
	"name":"AIC open field test arena",
	"shape":"round",
	"x_dim":390,
	"z_dim":347,
	"wall_color":"white",
	})

#Handling
add_generic(db_location, parameters={"CATEGORY":"HandlingHabituationProtocol", "code":"day1", "session_duration":10, "individual_picking_up":True, "group_picking_up":False, "transparent_tube":False})
add_generic(db_location, parameters={"CATEGORY":"HandlingHabituationProtocol", "code":"day2+", "session_duration":10, "individual_picking_up":True, "group_picking_up":True, "transparent_tube":False})

#scanner setups
add_generic(db_location, parameters={"CATEGORY":"FMRIScannerSetup", "code":"basic_ofm", "coil":"T/R ofMRI Mark 1", "scanner":"7T Pharmascan","support":"Bruker modular"})
add_generic(db_location, parameters={"CATEGORY":"FMRIScannerSetup", "code":"basic_ofm_nocoil", "scanner":"7T Pharmascan","support":"Bruker modular"})
add_generic(db_location, parameters={"CATEGORY":"FMRIScannerSetup", "code":"cryo", "coil":"cryo", "scanner":"7T Pharmascan", "support":"cryo"})
add_generic(db_location, parameters={"CATEGORY":"FMRIScannerSetup", "code":"cryo_support", "coil":"T/R ofMRI Mark 1", "scanner":"7T Pharmascan", "support":"cryo"})

#treatment protocols
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "solution_id":"Solution:code.iso3", "route":"inhalation"})
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "solution_id":"Solution:code.iso2", "route":"inhalation"})
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "solution_id":"Solution:code.iso05", "route":"inhalation"})
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "solution_id":"Solution:code.iso15", "route":"inhalation"})
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "solution_id":"Solution:code.med-sal", "route":"sc", "rate":20, "dose":3,
	"rate_unit_id":"MeasurementUnit:code.mul/s",
	"dose_unit_id":"MeasurementUnit:code.mul/g",
	})
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "solution_id":"Solution:code.mel-sal", "route":"sc", "rate":30, "dose":5,
	"rate_unit_id":"MeasurementUnit:code.mul/s",
	"dose_unit_id":"MeasurementUnit:code.mul/g",
	})
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "solution_id":"Solution:code.mel-sal", "route":"sc", "rate":30, "dose":2,
	"rate_unit_id":"MeasurementUnit:code.mul/s",
	"dose_unit_id":"MeasurementUnit:code.mul/g",
	})
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "solution_id":"Solution:code.med-sal", "route":"sc", "rate":20, "dose":1.5,
	"rate_unit_id":"MeasurementUnit:code.mul/s",
	"dose_unit_id":"MeasurementUnit:code.mul/g",
	})
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "solution_id":"Solution:code.med-sal", "route":"iv", "rate":20, "dose":1.5,
	"rate_unit_id":"MeasurementUnit:code.mul/s",
	"dose_unit_id":"MeasurementUnit:code.mul/g",
	})
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "solution_id":"Solution:code.panc-sal", "route":"sc", "dose":0.5,
	"dose_unit_id":"MeasurementUnit:code.mul/g",
	})
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "solution_id":"Solution:code.panc_200-sal", "route":"sc", "dose":2,
	"dose_unit_id":"MeasurementUnit:code.mul/g",
	})
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "solution_id":"Solution:code.med-sal", "route":"sc", "rate":6,
	"rate_unit_id":"MeasurementUnit:code.mul/g/h",
	})
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "solution_id":"Solution:code.med-sal", "route":"sc", "rate":3,
	"rate_unit_id":"MeasurementUnit:code.mul/g/h",
	})
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "solution_id":"Solution:code.med-sal", "route":"iv", "rate":3,
	"rate_unit_id":"MeasurementUnit:code.mul/g/h",
	})
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "code":"cFluIP", "name":"chronic fluoxetine intraperitoneal", "frequency":"daily", "route":"ip", "dose":4.4, "dose_unit_id":"MeasurementUnit:code.mul/g", "solution_id":"Solution:code.fluIVP"})
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "code":"aFluIP", "name":"acute fluoxetine intraperitoneal", "route":"iv", "dose":4.4, "dose_unit_id":"MeasurementUnit:code.mul/g", "solution_id":"Solution:code.fluIVP"})
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "code":"aFluIV", "name":"acute fluoxetine intravenous", "route":"iv", "dose":4.4, "dose_unit_id":"MeasurementUnit:code.mul/g", "solution_id":"Solution:code.fluIVP"})
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "code":"aFluIV_", "name":"acute fluoxetine intravenous - control", "route":"iv", "dose":4.4, "dose_unit_id":"MeasurementUnit:code.mul/g", "solution_id":"Solution:code.sal"})
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "code":"aFluSC", "name":"acute fluoxetine subcutaneous", "route":"iv", "dose":4.4, "dose_unit_id":"MeasurementUnit:code.mul/g", "solution_id":"Solution:code.fluIVP"})
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "code":"cFluDW", "name":"chronic fluoxetine in drinking water", "route":"dw", "solution_id":"Solution:code.fluDW"})
add_generic(db_location, parameters={"CATEGORY":"TreatmentProtocol", "code":"cFluDW_", "name":"chronic fluoxetine in drinking water - control", "route":"dw", "solution_id":"Solution:code.tw"})

#fmri preparations
add_generic(db_location, parameters={"CATEGORY":"AnesthesiaProtocol", "code":"MscIfb", "name":"Medatomidine subcutaneous, Isoflurane free breathing", "respiration":"free", "authors":["Operator:code.SOE"], "bolus_to_maintenance_delay":300,
	"induction":[
		"TreatmentProtocol:solution_id.Solution:code.iso3",
		],
	"bolus":[
		"TreatmentProtocol:solution_id.Solution:code.iso15",
		"TreatmentProtocol:rate.20&&dose.3&&solution_id.Solution:code.med-sal",
		],
	"maintenance":[
		"TreatmentProtocol:solution_id.Solution:code.iso05",
		"TreatmentProtocol:rate.6&&solution_id.Solution:code.med-sal"
		],
	})
add_generic(db_location, parameters={"CATEGORY":"AnesthesiaProtocol", "code":"Ifb", "name":"Isoflurane free breathing", "respiration":"free", "authors":["Operator:code.YPI"],
	"induction":[
		"TreatmentProtocol:solution_id.Solution:code.iso3",
		],
	"maintenance":[
		"TreatmentProtocol:solution_id.Solution:code.iso2",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"AnesthesiaProtocol", "code":"MPivIi", "name":"Medatomidine/Pancuronium intravenous, Isoflurane intubated", "respiration":"intubated", "authors":["Operator:code.YSu"], "bolus_to_maintenance_delay":300,
	"induction":[
		"TreatmentProtocol:solution_id.Solution:code.iso3",
		],
	"bolus":[
		"TreatmentProtocol:rate.20&&dose.1.5&&solution_id.Solution:code.med-sal",
		],
	"maintenance":[
		"TreatmentProtocol:solution_id.Solution:code.iso05",
		"TreatmentProtocol:rate.3&&solution_id.Solution:code.med-sal",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"AnesthesiaProtocol", "code":"Ii", "name":"Isoflurane intubated", "respiration":"intubated", "authors":["Operator:code.YPI"],
	"induction":[
		"TreatmentProtocol:solution_id.Solution:code.iso3",
		],
	"maintenance":[
		"TreatmentProtocol:solution_id.Solution:code.iso15",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"AnesthesiaProtocol", "code":"MPscIi", "name":"Medatomidine/Pancuronium subcutaneous, Isoflurane intubated", "respiration":"intubated", "authors":["Operator:code.YPI"], "bolus_to_maintenance_delay":300,
	"induction":[
		"TreatmentProtocol:solution_id.Solution:code.iso3",
		],
	"bolus":[
		"TreatmentProtocol:rate.20&&dose.3&&solution_id.Solution:code.med-sal",
		"TreatmentProtocol:dose.0.5&&solution_id.Solution:code.panc-sal",
		],
	"maintenance":[
		"TreatmentProtocol:solution_id.Solution:code.iso05",
		"TreatmentProtocol:rate.6&&solution_id.Solution:code.med-sal",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"AnesthesiaProtocol", "code":"MP200scIi", "name":"Medatomidine/Pancuronium200 subcutaneous, Isoflurane intubated", "respiration":"intubated", "authors":["Operator:code.YPI"], "bolus_to_maintenance_delay":300,
	"induction":[
		"TreatmentProtocol:solution_id.Solution:code.iso3",
		],
	"bolus":[
		"TreatmentProtocol:rate.20&&dose.1.5&&solution_id.Solution:code.med-sal",
		"TreatmentProtocol:dose.2&&solution_id.Solution:code.panc_200-sal",
		],
	"maintenance":[
		"TreatmentProtocol:solution_id.Solution:code.iso05",
		"TreatmentProtocol:rate.3&&solution_id.Solution:code.med-sal",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"AnesthesiaProtocol", "code":"op", "name":"operation", "respiration":"free", "authors":["Operator:code.YPI"],
	"induction":[
		"TreatmentProtocol:solution_id.Solution:code.iso3",
		],
	"bolus":[
		"TreatmentProtocol:rate.30&&dose.5&&solution_id.Solution:code.mel-sal",
		],
	"maintenance":[
		"TreatmentProtocol:solution_id.Solution:code.iso2",
		],
	"recovery_bolus":[
		"TreatmentProtocol:rate.30&&dose.2&&solution_id.Solution:code.mel-sal",
		]
	})


#stereotactic targets

add_generic(db_location, parameters={"CATEGORY":"OrthogonalStereotacticTarget", "code":"dr_5684", "reference":"lambda",
	"posteroanterior":-0.6,
	"leftright":1,
	"superoinferior":3.6,
	"pitch": 20,
	})

add_generic(db_location, parameters={"CATEGORY":"OrthogonalStereotacticTarget", "code":"vta_dura", "reference":"bregma",
	"qualitative_depth_reference":"dura",
	"posteroanterior":-3.5,
	"leftright":0.5,
	"superoinferior":0,
	"depth":4,
	"yaw":0,
	})

add_generic(db_location, parameters={"CATEGORY":"OrthogonalStereotacticTarget", "code":"dr_skull", "reference":"lambda",
	"posteroanterior":-0.6,
	"leftright":1,
	"superoinferior":0.1,
	"depth":3.6,
	"yaw": 20,
	})
add_generic(db_location, parameters={"CATEGORY":"OrthogonalStereotacticTarget", "code":"dr_dura", "reference":"lambda",
	"qualitative_depth_reference":"dura",
	"posteroanterior":-0.6,
	"leftright":1,
	"superoinferior":0.4,
	"depth":3.5,
	"yaw": 20,
	})
add_generic(db_location, parameters={"CATEGORY":"OrthogonalStereotacticTarget", "code":"dr_dura_shallow", "reference":"lambda",
	"qualitative_depth_reference":"dura",
	"posteroanterior":-0.6,
	"leftright":1,
	"superoinferior":0.4,
	"depth":3.4,
	"yaw": 20,
	})
add_generic(db_location, parameters={"CATEGORY":"OrthogonalStereotacticTarget", "code":"dr_skull_perpendicular", "reference":"lambda",
	"posteroanterior":-0.6,
	"leftright":0,
	"superoinferior":0.1,
	"depth":3,
	"yaw": 0,
	})
add_generic(db_location, parameters={"CATEGORY":"OrthogonalStereotacticTarget", "code":"dr_impl", "reference":"lambda",
	"posteroanterior":-0.6,
	"leftright":0,
	"superoinferior":0.1,
	"depth":3.3,
	"yaw": 0,
	})
add_generic(db_location, parameters={"CATEGORY":"OrthogonalStereotacticTarget", "code":"vta_impl", "reference":"bregma",
	"posteroanterior":-3.5,
	"leftright":0.5,
	"superoinferior":-0.3,
	"depth":4,
	"yaw": 0,
	})
#operation protocols

add_generic(db_location, parameters={"CATEGORY":"VirusInjectionProtocol", "amount":1.5,
	"virus_id":"Virus:code.dflox.hChR2.YFP",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.vta_dura",
	"virus_injection_speed":200,
	"virus_diffusion_time":10,
	})
add_generic(db_location, parameters={"CATEGORY":"VirusInjectionProtocol", "amount":1,
	"virus_id":"Virus:code.dflox.hChR2.mCherry",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.dr_skull",
	"virus_injection_speed":100,
	"virus_diffusion_time":10,
	})
add_generic(db_location, parameters={"CATEGORY":"VirusInjectionProtocol", "amount":1.5,
	"virus_id":"Virus:code.dflox.hChR2.YFP",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.dr_skull",
	"virus_injection_speed":100,
	"virus_diffusion_time":10,
	})
add_generic(db_location, parameters={"CATEGORY":"VirusInjectionProtocol", "amount":1.5,
	"virus_id":"Virus:code.dflox.hChR2.YFP",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.dr_dura",
	"virus_injection_speed":200,
	"virus_diffusion_time":10,
	})
add_generic(db_location, parameters={"CATEGORY":"VirusInjectionProtocol", "amount":1.3,
	"virus_id":"Virus:code.dflox.hChR2.YFP",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.dr_dura_shallow",
	"virus_injection_speed":200,
	"virus_diffusion_time":10,
	})
add_generic(db_location, parameters={"CATEGORY":"VirusInjectionProtocol", "amount":1.5,
	"virus_id":"Virus:code.dflox.hChR2.YFP",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.dr_dura_shallow",
	"virus_injection_speed":200,
	"virus_diffusion_time":10,
	})
add_generic(db_location, parameters={"CATEGORY":"VirusInjectionProtocol", "amount":1,
	"virus_id":"Virus:code.dflox.hChR2.YFP",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.dr_skull_perpendicular",
	"virus_injection_speed":200,
	"virus_diffusion_time":10,
	})
add_generic(db_location, parameters={"CATEGORY":"VirusInjectionProtocol", "amount":1,
	"virus_id":"Virus:code.dflox.hChR2.mCherry",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.dr_skull_perpendicular",
	"virus_injection_speed":200,
	"virus_diffusion_time":10,
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplantProtocol",
	"optic_fiber_implant_id":"OpticFiberImplant:code.33gen",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.dr_impl",
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplantProtocol",
	"optic_fiber_implant_id":"OpticFiberImplant:length.2.6&&transmittance.25",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.dr_impl",
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplantProtocol",
	"optic_fiber_implant_id":"OpticFiberImplant:length.3.3&&transmittance.65",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.dr_impl",
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplantProtocol",
	"optic_fiber_implant_id":"OpticFiberImplant:length.3.3&&transmittance.70",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.dr_impl",
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplantProtocol",
	"optic_fiber_implant_id":"OpticFiberImplant:length.3.3&&transmittance.78",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.dr_impl",
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplantProtocol",
	"optic_fiber_implant_id":"OpticFiberImplant:length.3.3&&transmittance.80",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.dr_impl",
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplantProtocol",
	"optic_fiber_implant_id":"OpticFiberImplant:length.3.3&&transmittance.85",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.dr_impl",
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplantProtocol",
	"optic_fiber_implant_id":"OpticFiberImplant:length.3.3&&transmittance.90",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.dr_impl",
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplantProtocol",
	"optic_fiber_implant_id":"OpticFiberImplant:length.3.3&&transmittance.95",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.dr_impl",
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplantProtocol",
	"optic_fiber_implant_id":"OpticFiberImplant:length.3.3&&transmittance.98",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.dr_impl",
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplantProtocol",
	"optic_fiber_implant_id":"OpticFiberImplant:length.3.3&&transmittance.100",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.dr_impl",
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplantProtocol",
	"optic_fiber_implant_id":"OpticFiberImplant:length.4&&transmittance.90",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.vta_impl",
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplantProtocol",
	"optic_fiber_implant_id":"OpticFiberImplant:length.4&&transmittance.95",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.vta_impl",
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplantProtocol",
	"optic_fiber_implant_id":"OpticFiberImplant:length.4&&transmittance.95",
	"stereotactic_target_id":"OrthogonalStereotacticTarget:code.dr_5684",
	})


#stimulation protocols
add_generic(db_location, parameters={"CATEGORY":"StimulationProtocol", "code":"nostim"})
add_generic(db_location, parameters={"CATEGORY":"StimulationProtocol", "code":"jb_long",
	"events":[
		{'CATEGORY':'StimulationEvent', 'onset':222, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':402, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':582, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':762, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':942, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':1122, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		],
	})
add_generic(db_location, parameters={"CATEGORY":"StimulationProtocol", "code":"chr_vlongSOA",
	"events":[
		{'CATEGORY':'StimulationEvent', 'onset':192, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':362, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':532, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':702, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':872, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':1042, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':1212, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':1382, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		],
	})
add_generic(db_location, parameters={"CATEGORY":"StimulationProtocol", "code":"chr_longSOA",
	"events":[
		{'CATEGORY':'StimulationEvent', 'onset':192, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':342, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':492, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':642, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':792, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':942, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':1092, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':1242, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		],
	})
add_generic(db_location, parameters={"CATEGORY":"StimulationProtocol", "code":"chr_longSOA_bimod",
	"events":[
		{'CATEGORY':'StimulationEvent', 'onset':192, 'duration':15, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':342, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':492, 'duration':15, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':642, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':792, 'duration':15, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':942, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':1092, 'duration':15, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':1242, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		],
	})
add_generic(db_location, parameters={"CATEGORY":"StimulationProtocol", "code":"alej",
	"events":[
		{'CATEGORY':'StimulationEvent', 'onset':222, 'duration':30, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':372, 'duration':30, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':522, 'duration':30, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':672, 'duration':30, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':822, 'duration':30, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':972, 'duration':30, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		],
	})
add_generic(db_location, parameters={"CATEGORY":"StimulationProtocol", "code":"jin6",
	"events":[
		{'CATEGORY':'StimulationEvent', 'onset':222, 'duration':20, 'frequency':6, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':282, 'duration':20, 'frequency':6, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':342, 'duration':20, 'frequency':6, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':402, 'duration':20, 'frequency':6, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':462, 'duration':20, 'frequency':6, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':522, 'duration':20, 'frequency':6, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		],
	})
add_generic(db_location, parameters={"CATEGORY":"StimulationProtocol", "code":"jin10",
	"events":[
		{'CATEGORY':'StimulationEvent', 'onset':222, 'duration':20, 'frequency':10, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':282, 'duration':20, 'frequency':10, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':342, 'duration':20, 'frequency':10, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':402, 'duration':20, 'frequency':10, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':462, 'duration':20, 'frequency':10, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':522, 'duration':20, 'frequency':10, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		],
	})
add_generic(db_location, parameters={"CATEGORY":"StimulationProtocol", "code":"jin20",
	"events":[
		{'CATEGORY':'StimulationEvent', 'onset':222, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':282, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':342, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':402, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':462, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':522, 'duration':20, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		],
	})
add_generic(db_location, parameters={"CATEGORY":"StimulationProtocol", "code":"jin40",
	"events":[
		{'CATEGORY':'StimulationEvent', 'onset':222, 'duration':20, 'frequency':40, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':282, 'duration':20, 'frequency':40, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':342, 'duration':20, 'frequency':40, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':402, 'duration':20, 'frequency':40, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':462, 'duration':20, 'frequency':40, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':522, 'duration':20, 'frequency':40, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		],
	})
add_generic(db_location, parameters={"CATEGORY":"StimulationProtocol", "code":"jin60",
	"events":[
		{'CATEGORY':'StimulationEvent', 'onset':222, 'duration':20, 'frequency':60, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':282, 'duration':20, 'frequency':60, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':342, 'duration':20, 'frequency':60, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':402, 'duration':20, 'frequency':60, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':462, 'duration':20, 'frequency':60, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':522, 'duration':20, 'frequency':60, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		],
	})
add_generic(db_location, parameters={"CATEGORY":"StimulationProtocol", "code":"jp_phasic",
	"events":[
		{'CATEGORY':'StimulationEvent', 'onset':60, 'duration':1, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':100, 'duration':1, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':140, 'duration':1, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':180, 'duration':1, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':220, 'duration':1, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':260, 'duration':1, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':300, 'duration':1, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':340, 'duration':1, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':380, 'duration':1, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':420, 'duration':1, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':460, 'duration':1, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':500, 'duration':1, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':540, 'duration':1, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':580, 'duration':1, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		{'CATEGORY':'StimulationEvent', 'onset':620, 'duration':1, 'frequency':20, 'pulse_width':0.005, 'strength':30, 'unit_id':"MeasurementUnit:code.mW", 'wavelength':490},
		],
	})

#cell biology protocols
add_generic(db_location, parameters={"CATEGORY":"BrainExtractionProtocol", "code":"mpm", "name":"manual perfusion for microtome slicing", "perfusion_system":"syringe",
"authors":["Operator:code.YOCo"],
"flushing_solution_id":"Solution:code.PBS",
"flushing_solution_volume":10,
"fixation_solution_id":"Solution:code.4PFA",
"fixation_solution_volume":10,
"storage_solution_id":"Solution:code.1PFA",
"storage_solution_volume":10,
"post_extraction_fixation_time":23,
})
add_generic(db_location, parameters={"CATEGORY":"SectioningProtocol", "code":"vt200", "name":"vibratome 200 micrometre slices",
"system":"vibratome",
"start_lambda_distance":-1,
"slice_thickness":200,
"blade_frequency":60,
"blade_speed":100,
})
