#-*- coding: utf-8 -*-
from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'

#Operators

add_generic(db_location, parameters={"CATEGORY":"Operator", "code":"YPI", "full_name":"Your Personal Initials", "affiliation":"SomeAwesomeUniversity", "email":"initials@someawesomeuni.eu"})
add_generic(db_location, parameters={"CATEGORY":"Operator", "code":"SOE", "full_name":"Some-One Else", "affiliation":"ETH", "email":"else@biomed.ee.ethz.ch"})
add_generic(db_location, parameters={"CATEGORY":"Operator", "code":"YoC", "full_name":u"Your Collaborator", "affiliation":"ETH", "email":"collaborator@biomed.ee.ethz.ch"})
add_generic(db_location, parameters={"CATEGORY":"Operator", "code":"YSu", "full_name":"Your Supervisor", "affiliation":"ETH", "email":"your.supervisor@hest.ethz.ch"})
add_generic(db_location, parameters={"CATEGORY":"Operator", "code":"YOCo", "full_name":u"Your Other Collaborator", "affiliation":"ETH", "email":"collabrator@hest.ethz.ch"})
add_generic(db_location, parameters={"CATEGORY":"Operator", "code":"ExCol", "full_name":u"External Collaborator", "affiliation":"PUK", "email":"external.collabrator@bli.uzh.ch"})


#genotypes

add_generic(db_location, parameters={"CATEGORY":"Genotype", "code":"epwt", "construct":"ePet-cre", "zygosity":"wt"})
add_generic(db_location, parameters={"CATEGORY":"Genotype", "code":"eptg", "construct":"ePet-cre", "zygosity":"tg"})
add_generic(db_location, parameters={"CATEGORY":"Genotype", "code":"dawt", "construct":"DAT-cre", "zygosity":"wt"})
add_generic(db_location, parameters={"CATEGORY":"Genotype", "code":"datg", "construct":"DAT-cre", "zygosity":"tg"})

#Substances

add_generic(db_location, parameters={"CATEGORY":"Substance", "code":"tw", "name":"tap water"})
add_generic(db_location, parameters={"CATEGORY":"Substance", "code":"mpw", "name":"millipore water"})
add_generic(db_location, parameters={"CATEGORY":"Substance", "code":"hcl", "name":"hydrogen chloride"})
add_generic(db_location, parameters={"CATEGORY":"Substance", "code":"tris", "name":"Tris", "long_name":"tris(hydroxymethyl)aminomethane"})
add_generic(db_location, parameters={"CATEGORY":"Substance", "code":"edta", "name":"EDTA", "long_name":"ethylenediaminetetraacetic acid"})
add_generic(db_location, parameters={"CATEGORY":"Substance", "code":"sds", "name":"SDS", "long_name":"sodium dodecyl sulfate"})
add_generic(db_location, parameters={"CATEGORY":"Substance", "code":"flu", "name":"fluoxetine hydrochloride", "supplier":"Tocris"})
add_generic(db_location, parameters={"CATEGORY":"Substance", "code":"med", "name":"medetomidine", "supplier":"Provert AG, Orion Pharma", "supplier_product_code":"DOMITOR"})
add_generic(db_location, parameters={"CATEGORY":"Substance", "code":"panc", "name":"pancuronium bromide", "supplier":"SIGMA", "supplier_product_code":"P1918-100MG"})
add_generic(db_location, parameters={"CATEGORY":"Substance", "code":"nacl", "name":"table salt", "long_name":"sodium chloride"})
add_generic(db_location, parameters={"CATEGORY":"Substance", "code":"iso", "name":"isoflurane", "supplier":"Piramal Healthcare"})
add_generic(db_location, parameters={"CATEGORY":"Substance", "code":"air", "name":"air"})
add_generic(db_location, parameters={"CATEGORY":"Substance", "code":"o2", "name":"oxygen"})
add_generic(db_location, parameters={"CATEGORY":"Substance", "code":"rnaa", "name":"RNase A"})
add_generic(db_location, parameters={"CATEGORY":"Substance", "code":"pk", "name":"proteinse K", "supplier":"Thermo Fisher Scientific", "supplier_product_code":"EO0491", "concentration":20, "concentration_unit_id":"MeasurementUnit:code.mg/ml"})
add_generic(db_location, parameters={"CATEGORY":"Substance", "code":"pfa", "name":"paraformaldehyde"})
add_generic(db_location, parameters={"CATEGORY":"Substance", "code":"spmm", "name":"sodium phosphate monobasic monohydrate", "supplier":"SIGMA-ALDRICH", "supplier_product_code":"71504", "pubchem_sid":"24886208"})
add_generic(db_location, parameters={"CATEGORY":"Substance", "code":"spdd", "name":"sodium phosphate dibasic dihydrate", "supplier":"SIGMA-ALDRICH", "supplier_product_code":"30412",})
add_generic(db_location, parameters={"CATEGORY":"Substance", "code":"mel", "name":"meloxicam", "supplier":"Boehringer Ingelheim (Schweiz) GmbH", "supplier_product_code":"QM01AC06", "concentration":5, "concentration_unit_id":"MeasurementUnit:code.mg/ml"})


#Solutions:

add_generic(db_location, parameters={"CATEGORY":"Solution", "code":"mpw", "name":"millipore water", "contains":[{"CATEGORY":"Ingredient","substance_id":"Substance:code.mpw"}]})
add_generic(db_location, parameters={"CATEGORY":"Solution", "code":"tw", "name":"tap water", "contains":[{"CATEGORY":"Ingredient","substance_id":"Substance:code.tw"}]})
add_generic(db_location, parameters={"CATEGORY":"Solution", "code":"pk", "name":"proteinase K", "contains":[{"CATEGORY":"Ingredient","substance_id":"Substance:code.pk"}]})
add_generic(db_location, parameters={"CATEGORY":"Solution", "code":"EPqEB", "name":"Ear Punch quick-Extraction Buffer", "contains":[
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.tris","concentration":50, "concentration_unit_id":"MeasurementUnit:code.mM"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.hcl","concentration":50, "concentration_unit_id":"MeasurementUnit:code.mM"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.nacl","concentration":20, "concentration_unit_id":"MeasurementUnit:code.mM"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.sds","concentration":1, "concentration_unit_id":"MeasurementUnit:code.percent"},
	]})
add_generic(db_location, parameters={"CATEGORY":"Solution", "code":"med-sal", "name":"medetomidine in saline", "contains":[
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.med","concentration":33.3, "concentration_unit_id":"MeasurementUnit:code.mg/l"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.nacl","concentration":8.7, "concentration_unit_id":"MeasurementUnit:code.g/l"},
	]})
add_generic(db_location, parameters={"CATEGORY":"Solution", "code":"mel-sal", "name":"meloxicam in saline", "contains":[
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.mel","concentration":0.5, "concentration_unit_id":"MeasurementUnit:code.mg/ml"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.nacl","concentration":8.1, "concentration_unit_id":"MeasurementUnit:code.g/l"},
	]})
add_generic(db_location, parameters={"CATEGORY":"Solution", "code":"sal", "name":"saline", "contains":[
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.nacl","concentration":9, "concentration_unit_id":"MeasurementUnit:code.g/l"},
	]})
add_generic(db_location, parameters={"CATEGORY":"Solution", "code":"panc-sal", "name":"pancuronium in saline stock", "contains":[
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.panc","concentration":1, "concentration_unit_id":"MeasurementUnit:code.g/l"},
	"Ingredient:substance_id.Substance:code.nacl&&concentration.8.7&&concentration_unit_id.MeasurementUnit:code.g/l",
	]})
add_generic(db_location, parameters={"CATEGORY":"Solution", "code":"panc_200-sal", "name":"pancuronium in saline dilution", "contains":[
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.panc","concentration":200, "concentration_unit_id":"MeasurementUnit:code.mg/l"},
	"Ingredient:substance_id.Substance:code.nacl&&concentration.8.7&&concentration_unit_id.MeasurementUnit:code.g/l",
	]})
add_generic(db_location, parameters={"CATEGORY":"Solution", "code":"iso3", "name":"3% isoflurane", "contains":[
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.air","concentration":77.6, "concentration_unit_id":"MeasurementUnit:code.percent"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.o2","concentration":19.4, "concentration_unit_id":"MeasurementUnit:code.percent"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.iso","concentration":3, "concentration_unit_id":"MeasurementUnit:code.percent"},
	]})
add_generic(db_location, parameters={"CATEGORY":"Solution", "code":"iso2", "name":"2% isoflurane", "contains":[
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.air","concentration":78.4, "concentration_unit_id":"MeasurementUnit:code.percent"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.o2","concentration":19.6, "concentration_unit_id":"MeasurementUnit:code.percent"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.iso","concentration":2, "concentration_unit_id":"MeasurementUnit:code.percent"},
	]})
add_generic(db_location, parameters={"CATEGORY":"Solution", "code":"iso15", "name":"1.5% isoflurane", "contains":[
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.air","concentration":79.2, "concentration_unit_id":"MeasurementUnit:code.percent"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.o2","concentration":19.8, "concentration_unit_id":"MeasurementUnit:code.percent"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.iso","concentration":1, "concentration_unit_id":"MeasurementUnit:code.percent"},
	]})
add_generic(db_location, parameters={"CATEGORY":"Solution", "code":"iso05", "name":"0.5% isoflurane", "contains":[
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.air","concentration":79.6, "concentration_unit_id":"MeasurementUnit:code.percent"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.o2","concentration":19.9, "concentration_unit_id":"MeasurementUnit:code.percent"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.iso","concentration":0.5, "concentration_unit_id":"MeasurementUnit:code.percent"},
	]})
add_generic(db_location, parameters={"CATEGORY":"Solution", "code":"RA10", "name":"10% RNase A", "contains":[
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.rnaa","concentration":10, "concentration_unit_id":"MeasurementUnit:code.mg/ml"},
	]})
add_generic(db_location, parameters={"CATEGORY":"Solution", "code":"fluIVP", "name":"fluoxetine in saline", "contains":[
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.flu","concentration":2.25, "concentration_unit_id":"MeasurementUnit:code.mg/ml"},
	"Ingredient:substance_id.Substance:code.nacl&&concentration.8.7&&concentration_unit_id.MeasurementUnit:code.g/l",
	]})
add_generic(db_location, parameters={"CATEGORY":"Solution", "code":"fluDW", "name":"fluoxetine in tap water", "contains":[
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.flu","concentration":124, "concentration_unit_id":"MeasurementUnit:code.mg/l"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.tw"},
	]})
add_generic(db_location, parameters={"CATEGORY":"Solution", "code":"4PFA", "name":"4% paraformaldehyde in phosphare buffer", "contains":[
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.pfa","concentration":40, "concentration_unit_id":"MeasurementUnit:code.g/l"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.spmm","concentration":3.18, "concentration_unit_id":"MeasurementUnit:code.g/l"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.spmm","concentration":13.73, "concentration_unit_id":"MeasurementUnit:code.g/l"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.mpw"},
	]})
add_generic(db_location, parameters={"CATEGORY":"Solution", "code":"1PFA", "name":"1% paraformaldehyde in phosphare buffer", "contains":[
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.pfa","concentration":10, "concentration_unit_id":"MeasurementUnit:code.g/l"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.spmm","concentration":3.18, "concentration_unit_id":"MeasurementUnit:code.g/l"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.spmm","concentration":13.73, "concentration_unit_id":"MeasurementUnit:code.g/l"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.mpw"},
	]})
add_generic(db_location, parameters={"CATEGORY":"Solution", "code":"PBS", "name":"phosphate buffered saline", "contains":[
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.nacl","concentration":9, "concentration_unit_id":"MeasurementUnit:code.g/l"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.spmm","concentration":3.18, "concentration_unit_id":"MeasurementUnit:code.g/l"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.spmm","concentration":13.73, "concentration_unit_id":"MeasurementUnit:code.g/l"},
	{"CATEGORY":"Ingredient","substance_id":"Substance:code.mpw"},
	]})


#viruses

add_generic(db_location, parameters={"CATEGORY":"Virus", "code":"dflox.hChR2.mCherry"})
add_generic(db_location, parameters={"CATEGORY":"Virus", "code":"dflox.hChR2.YFP"})


#implants

add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplant", "code":"33gen",
	"long_code":"MFC 400/480-0.22 3.3mm ZF1.25_FLT",
	"ferrule_diameter":1.25,
	"length":3.3,
	"manufacturer":"Doric",
	"manufacturer_code":"B280-4205-2",
	"numerical_apperture":0.22,
	})

add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplant",
	"long_code":"MFC 400/480-0.22 2.6mm ZF1.25_FLT",
	"ferrule_diameter":1.25,
	"length":2.6,
	"manufacturer":"Doric",
	"numerical_apperture":0.22,
	"transmittance":25,
	})

# DR implants
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplant",
	"long_code":"MFC 400/480-0.22 3.3mm ZF1.25_FLT",
	"ferrule_diameter":1.25,
	"length":3.3,
	"manufacturer":"Doric",
	"manufacturer_code":"B280-4205-2",
	"numerical_apperture":0.22,
	"transmittance":65,
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplant",
	"long_code":"MFC 400/480-0.22 3.3mm ZF1.25_FLT",
	"ferrule_diameter":1.25,
	"length":3.3,
	"manufacturer":"Doric",
	"manufacturer_code":"B280-4205-2",
	"numerical_apperture":0.22,
	"transmittance":70,
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplant",
	"long_code":"MFC 400/480-0.22 3.3mm ZF1.25_FLT",
	"ferrule_diameter":1.25,
	"length":3.3,
	"manufacturer":"Doric",
	"manufacturer_code":"B280-4205-2",
	"numerical_apperture":0.22,
	"transmittance":78,
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplant",
	"long_code":"MFC 400/480-0.22 3.3mm ZF1.25_FLT",
	"ferrule_diameter":1.25,
	"length":3.3,
	"manufacturer":"Doric",
	"manufacturer_code":"B280-4205-2",
	"numerical_apperture":0.22,
	"transmittance":80,
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplant",
	"long_code":"MFC 400/480-0.22 3.3mm ZF1.25_FLT",
	"ferrule_diameter":1.25,
	"length":3.3,
	"manufacturer":"Doric",
	"manufacturer_code":"B280-4205-2",
	"numerical_apperture":0.22,
	"transmittance":85,
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplant",
	"long_code":"MFC 400/480-0.22 3.3mm ZF1.25_FLT",
	"ferrule_diameter":1.25,
	"length":3.3,
	"manufacturer":"Doric",
	"manufacturer_code":"B280-4205-2",
	"numerical_apperture":0.22,
	"transmittance":90,
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplant",
	"long_code":"MFC 400/480-0.22 3.3mm ZF1.25_FLT",
	"ferrule_diameter":1.25,
	"length":3.3,
	"manufacturer":"Doric",
	"manufacturer_code":"B280-4205-2",
	"numerical_apperture":0.22,
	"transmittance":95,
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplant",
	"long_code":"MFC 400/480-0.22 3.3mm ZF1.25_FLT",
	"ferrule_diameter":1.25,
	"length":3.3,
	"manufacturer":"Doric",
	"manufacturer_code":"B280-4205-2",
	"numerical_apperture":0.22,
	"transmittance":98,
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplant",
	"long_code":"MFC 400/480-0.22 3.3mm ZF1.25_FLT",
	"ferrule_diameter":1.25,
	"length":3.3,
	"manufacturer":"Doric",
	"manufacturer_code":"B280-4205-2",
	"numerical_apperture":0.22,
	"transmittance":100,
	})
#VTA
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplant",
	"long_code":"MFC 400/480-0.22 4mm ZF1.25_FLT",
	"ferrule_diameter":1.25,
	"length":4,
	"manufacturer":"Doric",
	"numerical_apperture":0.22,
	"transmittance":90,
	})
add_generic(db_location, parameters={"CATEGORY":"OpticFiberImplant",
	"long_code":"MFC 400/480-0.22 4mm ZF1.25_FLT",
	"ferrule_diameter":1.25,
	"length":4,
	"manufacturer":"Doric",
	"numerical_apperture":0.22,
	"transmittance":95,
	})
