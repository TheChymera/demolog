from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'
#create cages up to (but not including) last integer
for i in range(25,58):
	add_generic(db_location, parameters={"CATEGORY":"Cage", "id":i, "location":"AIC"})

append_parameter(db_location, entry_identification="Cage:id.57",parameters={"environmental_enrichment":"house"})

#cage_stays
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2016,10,18", "cage_id":"Cage:id.25"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2017,1,4,16,30", "cage_id":"Cage:id.25"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2016,10,18", "cage_id":"Cage:id.26"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2016,10,18", "cage_id":"Cage:id.27"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2017,1,4,16,30", "cage_id":"Cage:id.27"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2016,10,18", "cage_id":"Cage:id.28"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2016,12,6", "cage_id":"Cage:id.29"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2016,12,6", "cage_id":"Cage:id.30"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2016,12,6", "cage_id":"Cage:id.31"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2016,12,29,21", "cage_id":"Cage:id.32"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2016,12,29,21", "cage_id":"Cage:id.33"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2016,12,29,21", "cage_id":"Cage:id.34"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2016,12,29,21", "cage_id":"Cage:id.35"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2016,12,29,21", "cage_id":"Cage:id.36"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2017,1,10", "cage_id":"Cage:id.37"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2017,1,10", "cage_id":"Cage:id.38"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2017,1,10", "cage_id":"Cage:id.39"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2017,1,10", "cage_id":"Cage:id.40"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2017,1,10", "cage_id":"Cage:id.41"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2017,3,13", "cage_id":"Cage:id.40"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2017,3,13", "cage_id":"Cage:id.41"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2017,1,10", "cage_id":"Cage:id.42"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2017,2,3,16", "cage_id":"Cage:id.43"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2017,2,21,17", "cage_id":"Cage:id.44"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2017,2,21,17", "cage_id":"Cage:id.45"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2016,11,1", "cage_id":"Cage:id.50"})
add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2017,5,5", "cage_id":"Cage:id.57"})

for i in range (51,57):
	add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2017,3,3,21,15", "cage_id":"Cage:id."+str(i)})


##handling_habituations
for i in range(25,29):
	add_generic(db_location, parameters={"CATEGORY":"HandlingHabituation", "date":"2016,10,18,11", "cage_id":"Cage:id."+str(i), "protocol_id":"HandlingHabituationProtocol:code.day1"})
	add_generic(db_location, parameters={"CATEGORY":"HandlingHabituation", "date":"2016,10,19,13", "cage_id":"Cage:id."+str(i), "protocol_id":"HandlingHabituationProtocol:code.day2+"})
	add_generic(db_location, parameters={"CATEGORY":"HandlingHabituation", "date":"2016,10,20,18", "cage_id":"Cage:id."+str(i), "protocol_id":"HandlingHabituationProtocol:code.day2+"})
for i in range(29,32):
	add_generic(db_location, parameters={"CATEGORY":"HandlingHabituation", "date":"2016,12,7", "cage_id":"Cage:id."+str(i), "protocol_id":"HandlingHabituationProtocol:code.day1"})
	add_generic(db_location, parameters={"CATEGORY":"HandlingHabituation", "date":"2016,12,8", "cage_id":"Cage:id."+str(i), "protocol_id":"HandlingHabituationProtocol:code.day2+"})
	add_generic(db_location, parameters={"CATEGORY":"HandlingHabituation", "date":"2016,12,9", "cage_id":"Cage:id."+str(i), "protocol_id":"HandlingHabituationProtocol:code.day2+"})
for i in range(37,42):
	add_generic(db_location, parameters={"CATEGORY":"HandlingHabituation", "date":"2017,1,11", "cage_id":"Cage:id."+str(i), "protocol_id":"HandlingHabituationProtocol:code.day2+"})
	add_generic(db_location, parameters={"CATEGORY":"HandlingHabituation", "date":"2017,1,12", "cage_id":"Cage:id."+str(i), "protocol_id":"HandlingHabituationProtocol:code.day2+"})
	add_generic(db_location, parameters={"CATEGORY":"HandlingHabituation", "date":"2017,1,13", "cage_id":"Cage:id."+str(i), "protocol_id":"HandlingHabituationProtocol:code.day2+"})
for i in range(46,50):
	add_generic(db_location, parameters={"CATEGORY":"CageStay","start_date":"2017,2,28,14", "cage_id":"Cage:id."+str(i)})
	add_generic(db_location, parameters={"CATEGORY":"HandlingHabituation", "date":"2017,3,1,19", "cage_id":"Cage:id."+str(i), "protocol_id":"HandlingHabituationProtocol:code.day2+"})
	add_generic(db_location, parameters={"CATEGORY":"HandlingHabituation", "date":"2017,3,2", "cage_id":"Cage:id."+str(i), "protocol_id":"HandlingHabituationProtocol:code.day2+"})
	add_generic(db_location, parameters={"CATEGORY":"HandlingHabituation", "date":"2017,3,3", "cage_id":"Cage:id."+str(i), "protocol_id":"HandlingHabituationProtocol:code.day2+"})
	add_generic(db_location, parameters={"CATEGORY":"HandlingHabituation", "date":"2017,3,4", "cage_id":"Cage:id."+str(i), "protocol_id":"HandlingHabituationProtocol:code.day2+"})

add_generic(db_location, parameters={"CATEGORY":"HandlingHabituation", "date":"2017,1,11", "cage_id":"Cage:id.42", "protocol_id":"HandlingHabituationProtocol:code.day1"})
add_generic(db_location, parameters={"CATEGORY":"HandlingHabituation", "date":"2017,1,12", "cage_id":"Cage:id.42", "protocol_id":"HandlingHabituationProtocol:code.day2+"})
add_generic(db_location, parameters={"CATEGORY":"HandlingHabituation", "date":"2017,1,13", "cage_id":"Cage:id.42", "protocol_id":"HandlingHabituationProtocol:code.day2+"})
