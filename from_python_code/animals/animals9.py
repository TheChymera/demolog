from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'
add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"m", "ear_punches":"L",
	"license":"666/2013",
	"birth_date":"2016,10,26",
	"death_reason":"end of experiment",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"6254"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M4727"},
		],
	"genotypes":["Genotype:code.epwt"],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.46",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"m", "ear_punches":"R",
	"license":"666/2013",
	"birth_date":"2016,10,26",
	"death_reason":"end of experiment",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"6255"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M4728"},
		],
	"genotypes":["Genotype:code.eptg"],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.46",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"m", "ear_punches":"LR",
	"license":"666/2013",
	"birth_date":"2016,10,26",
	"death_reason":"end of experiment",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"6256"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M4729"},
		],
	"genotypes":["Genotype:code.epwt"],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.46",
		],
	})

add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"f", "ear_punches":"L",
	"license":"666/2013",
	"birth_date":"2016,11,1",
	"death_reason":"end of experiment",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"6261"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M4835"},
		],
	"genotypes":["Genotype:code.eptg"],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.48",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"f", "ear_punches":"R",
	"license":"666/2013",
	"birth_date":"2016,11,1",
	"death_reason":"end of experiment",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"6262"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M4836"},
		],
	"genotypes":["Genotype:code.eptg"],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.48",
		"CageStay:cage_id.Cage:id.57",
		],
	"observations":[
		{"CATEGORY":"Observation","date":"2017,5,26,9,0","physiology":"tail remainder", "value":60, "unit_id":"MeasurementUnit:code.mm",},
		],
	})

add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"m", "ear_punches":"R",
	"license":"666/2013",
	"birth_date":"2016,10,12",
	"death_reason":"end of experiment",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"6258"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M5458"},
		],
	"genotypes":["Genotype:code.datg"],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.47",
		],
	})
