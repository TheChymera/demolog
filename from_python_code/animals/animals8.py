from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'
add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"m", "ear_punches":"L",
	"license":"666/2013",
	"birth_date":"2016,8,22",
	"death_reason":"end of experiment",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5698"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M3584"},
		],
	"genotypes":["Genotype:code.eptg"],
	"measurements":[
		{"CATEGORY":"WeightMeasurement","date":"2017,1,17,23,0","weight":37.2, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,1,24,18,10","weight":37.7, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,1,31,10,45","weight":37.4, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,8,13","weight":37.5, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YoC"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,14,22","weight":36.0, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,21,16,30","weight":38.1, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,28,12,15","weight":39.0, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.38",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"m", "ear_punches":"R",
	"license":"666/2013",
	"birth_date":"2016,8,22",
	"death_reason":"end of experiment",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5699"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M3585"},
		],
	"genotypes":["Genotype:code.eptg"],
	"measurements":[
		{"CATEGORY":"WeightMeasurement","date":"2017,1,17,23,0","weight":34.4, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,1,24,18,10","weight":35.1, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,1,31,10,45","weight":34.1, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,8,13","weight":34.3, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YoC"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,14,22","weight":33.5, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,21,16,30","weight":33.8, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,28,12,15","weight":34.1, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.38",
		],
	"observations":[
		{"CATEGORY":"Observation","date":"2017,5,26,9,0","physiology":"tail remainder", "value":33, "unit_id":"MeasurementUnit:code.mm",},
		],
	})
add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"m", "ear_punches":"LR",
	"license":"666/2013",
	"birth_date":"2016,8,22",
	"death_reason":"end of experiment",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5700"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M3586"},
		],
	"genotypes":["Genotype:code.eptg"],
	"measurements":[
		{"CATEGORY":"WeightMeasurement","date":"2017,1,17,23,0","weight":36.1, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,1,24,18,10","weight":37.2, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,1,31,10,45","weight":37.5, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,8,13","weight":37.4, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YoC"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,14,22","weight":36.9, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,21,16,30","weight":38.3, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,28,12,15","weight":38.8, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.38",
		],
	})

add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"f", "ear_punches":"L",
	"license":"666/2013",
	"birth_date":"2016,8,22",
	"death_reason":"end of experiment",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5692"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M3572"},
		],
	"genotypes":["Genotype:code.eptg"],
	"measurements":[
		{"CATEGORY":"WeightMeasurement","date":"2017,1,17,23,0","weight":25.1, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,1,24,18,10","weight":25.5, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,1,31,10,45","weight":25.0, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,8,13","weight":25.4, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YoC"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,14,22","weight":25.0, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,21,16,30","weight":25.8, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,28,12,15","weight":25.0, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.42",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"f", "ear_punches":"R",
	"license":"666/2013",
	"birth_date":"2016,8,22",
	"death_reason":"end of experiment",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5693"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M3573"},
		],
	"genotypes":["Genotype:code.epwt"],
	"measurements":[
		{"CATEGORY":"WeightMeasurement","date":"2017,1,17,23,0","weight":24.6, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,1,24,18,10","weight":24.3, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,1,31,10,45","weight":25.1, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,8,13","weight":24.9, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YoC"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,14,22","weight":24.2, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,21,16,30","weight":25.1, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,28,12,15","weight":25.4, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.42",
		"CageStay:cage_id.Cage:id.44",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"f", "ear_punches":"LR",
	"license":"666/2013",
	"birth_date":"2016,8,22",
	"death_reason":"end of experiment",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5694"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M3574"},
		],
	"genotypes":["Genotype:code.eptg"],
	"measurements":[
		{"CATEGORY":"WeightMeasurement","date":"2017,1,17,23,0","weight":26.1, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,1,24,18,10","weight":25.5, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,1,31,10,45","weight":26.4, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,8,13","weight":26.4, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YoC"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,14,22","weight":25.6, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,21,16,30","weight":26.6, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,28,12,15","weight":25.5, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.42",
		"CageStay:cage_id.Cage:id.45",
		],
	})
