from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'
add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"m", "ear_punches":"L",
	"license":"666/2013",
	"birth_date":"2016,7,19",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5667"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M2883"},
		],
	"genotypes":["Genotype:code.eptg"],
	"measurements":[
		{"CATEGORY":"WeightMeasurement","date":"2016,10,27,12,57","weight":32.8, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,4,17,30","weight":32.8, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,10,11,12","weight":33.8, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,17,14,15","weight":33.2, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,24,20,26","weight":32.9, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,12,1,13,35","weight":31.8, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,12,8,13,55","weight":33.2, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,12,14,20,30","weight":32.4, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,12,22,14,28","weight":31.7, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,10,27,12,57,13",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"stimulations":["StimulationProtocol:code.chr_longSOA"],
			"temperature":36.8,
			"irregularities":[
				"Irregularity:description.CBV fMRI post-enorem delay 15 minutes longer than protocol",
				"Irregularity:description.no delay in between induction and maintenance anesthesia",
				"Irregularity:description.ICA failed to indicate response to stimulus",
				],
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,11,10,11,12,22",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"stimulations":["StimulationProtocol:code.chr_vlongSOA"],
			"temperature":36.4,
			"irregularities":["Irregularity:description.ICA failed to indicate response to stimulus"],
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,11,24,20,26,25",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"temperature":36,
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,12,8,13,55,10",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"temperature":37.5,
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,12,22,14,28,0",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"temperature":36.6,
			"irregularities":[
				"Irregularity:description.incomplete maintenance anesthetic delivery",
				],
			},
		],
	"observations":[
		{"CATEGORY":"Observation","date":"2016,11,17,14,15","physiology":"tail remainder", "value":77, "unit_id":"MeasurementUnit:code.mm",},
		],
	"cage_stays":[
		"CageStay:start_date.2016,10,18&&cage_id.Cage:id.25",
		"CageStay:cage_id.Cage:id.32",
		"CageStay:start_date.2017,1,4,16,30&&cage_id.Cage:id.25",
		],
	})

add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"m", "ear_punches":"R",
	"license":"666/2013",
	"birth_date":"2016,7,19",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5668"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M2884"},
		],
	"genotypes":["Genotype:code.eptg"],
	"measurements":[
		{"CATEGORY":"WeightMeasurement","date":"2016,10,27,14,44","weight":33, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,4,17,30","weight":30.9, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,10,12,50","weight":30.8, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,17,14,15","weight":31.4, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,24,19,52","weight":30.9, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,12,1,13,38","weight":31.0, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,12,8,15,28","weight":31.7, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,12,14,20,30","weight":31.5, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,12,22,15,17","weight":31.3, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,10,27,14,44,17",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"stimulations":["StimulationProtocol:code.chr_longSOA"],
			"temperature":35.8,
			"irregularities":["Irregularity:description.ICA failed to indicate response to stimulus"],
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,11,10,12,50,33",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"stimulations":["StimulationProtocol:code.jb_long"],
			"temperature":36.9,
			"irregularities":["Irregularity:description.ICA failed to indicate response to stimulus"],
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,11,24,19,52,55",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"temperature":35.3,
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,12,8,15,28,14",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"temperature":36.9,
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,12,22,15,17,2",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"temperature":35.6,
			},
		],
	"observations":[
		{"CATEGORY":"Observation","date":"2016,11,17,14,15","physiology":"tail remainder", "value":81, "unit_id":"MeasurementUnit:code.mm",},
		],
	"cage_stays":[
		"CageStay:start_date.2016,10,18&&cage_id.Cage:id.25",
		"CageStay:cage_id.Cage:id.33",
		"CageStay:start_date.2017,1,4,16,30&&cage_id.Cage:id.25",
		],
	})

add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"m", "ear_punches":"L",
	"license":"666/2013",
	"birth_date":"2016,7,15",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5672"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M2903"},
		],
	"genotypes":["Genotype:code.epwt"],
	"measurements":[
		{"CATEGORY":"WeightMeasurement","date":"2016,10,27,16,36","weight":30, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,4,17,30","weight":29.2, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,10,14,13","weight":30.1, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,17,14,15","weight":28.4, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,25,15,38","weight":28.9, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,10,27,16,36,11",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"stimulations":["StimulationProtocol:code.chr_longSOA"],
			"temperature":36.1,
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,11,10,14,13,37",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"irregularities":[
				"Irregularity:description.cannulation failed",
				],
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,11,24,15,38,5",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"irregularities":[
				"Irregularity:description.died during measurement",
				],
			},
		],
	"observations":[
		{"CATEGORY":"Observation","date":"2016,11,17,14,15","physiology":"tail remainder", "value":23, "unit_id":"MeasurementUnit:code.mm",},
		],
	"cage_stays":[
		"CageStay:start_date.2016,10,18&&cage_id.Cage:id.27",
		],
	})

add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"m", "ear_punches":"R",
	"license":"666/2013",
	"birth_date":"2016,7,15",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5673"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M2904"},
		],
	"genotypes":["Genotype:code.eptg"],
	"measurements":[
		{"CATEGORY":"WeightMeasurement","date":"2016,10,27,17,45","weight":30, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,4,17,30","weight":28.2, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,10,15,27","weight":29.4, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,17,14,15","weight":28.9, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,24,17,25","weight":28.7, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,12,1,13,45","weight":28.9, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,12,8,16,20","weight":28.5, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,12,14,20,30","weight":28.7, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,12,22,16,1","weight":28.3, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,10,27,17,45,45",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"stimulations":["StimulationProtocol:code.chr_longSOA"],
			"temperature":36.7,
			"irregularities":[
				"Irregularity:description.laser power low (30mW)",
				"Irregularity:description.ICA failed to indicate response to stimulus",
				],
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,11,24,17,25,23",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"temperature":38.2,
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,12,8,16,20,15",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"temperature":37.6,
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,12,22,16,1,32",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"temperature":36.8,
			},
		],
	"observations":[
		{"CATEGORY":"Observation","date":"2016,11,17,14,15","physiology":"tail remainder", "value":25, "unit_id":"MeasurementUnit:code.mm",},
		],
	"cage_stays":[
		"CageStay:start_date.2016,10,18&&cage_id.Cage:id.27",
		"CageStay:cage_id.Cage:id.34",
		"CageStay:start_date.2017,1,4,16,30&&cage_id.Cage:id.27",
		"CageStay:start_date.2017,2,3,16&&cage_id.Cage:id.43",
		],
	})

add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"m", "ear_punches":"LR",
	"birth_date":"2016,7,15",
	"license":"666/2013",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5674"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M2905"},
		],
	"genotypes":["Genotype:code.eptg"],
	"measurements":[
		{"CATEGORY":"WeightMeasurement","date":"2016,10,27,19,12","weight":31.4, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,4,17,30","weight":30.9, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,10,15,58","weight":31.5, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,17,14,15","weight":31.7, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,24,18,7","weight":31.3, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,12,1,13,45","weight":31.4, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,12,8,16,54","weight":31.2, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,12,14,20,30","weight":31.5, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,12,22,16,46","weight":30.3, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,10,27,19,12,24",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"stimulations":["StimulationProtocol:code.chr_longSOA"],
			"temperature":36.3,
			"irregularities":["Irregularity:description.ICA failed to indicate response to stimulus"]
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,11,10,15,58,26",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"stimulations":["StimulationProtocol:code.chr_longSOA"],
			"temperature":37.6,
			"irregularities":["Irregularity:description.ICA failed to indicate response to stimulus"]
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,11,24,18,7,27",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"temperature":36.8,
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,12,8,16,54,46",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"temperature":36.5,
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,12,22,16,46,41",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			},
		],
	"observations":[
		{"CATEGORY":"Observation","date":"2016,11,17,14,15","physiology":"tail remainder", "value":32, "unit_id":"MeasurementUnit:code.mm",},
		{"CATEGORY":"Observation","date":"2017,2,3","physiology":"implant chewed off",},
		],
	"cage_stays":[
		"CageStay:start_date.2016,10,18&&cage_id.Cage:id.27",
		"CageStay:cage_id.Cage:id.35",
		"CageStay:start_date.2017,1,4,16,30&&cage_id.Cage:id.27",
		],
	})

add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"m", "ear_punches":"2L",
	"license":"666/2013",
	"birth_date":"2016,7,15",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5675"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M2906"},
		],
	"genotypes":["Genotype:code.eptg"],
	"measurements":[
		{"CATEGORY":"WeightMeasurement","date":"2016,10,27,20,31","weight":27.9, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,4,17,30","weight":27.9, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,10,17,38","weight":28.8, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,17,14,15","weight":28.7, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,17,14,15","weight":29.1, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,12,1,13,45","weight":28.4, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,12,8,17,33","weight":27.8, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,12,14,20,30","weight":28.8, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,12,22,17,25","weight":29.4, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,2,13,14,42","weight":29.0, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,10,27,20,31,41",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"stimulations":["StimulationProtocol:code.chr_longSOA"],
			"temperature":36.1,
			"irregularities":[
				"Irregularity:description.slight measurement error in induction medetomidine administration (could be either too much or too little)",
				"Irregularity:description.ICA failed to indicate response to stimulus",
				],
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,11,10,17,38,28",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"stimulations":["StimulationProtocol:code.jb_long"],
			"temperature":37.3,
			"irregularities":["Irregularity:description.ICA failed to indicate response to stimulus"]
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,11,10,17,38,28",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"temperature":37.8,
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,12,8,17,33,33",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"temperature":37.3,
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,12,22,17,25,56",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"temperature":34.8,
			},
		],
	"observations":[
		{"CATEGORY":"Observation","date":"2016,11,17,14,15","physiology":"tail remainder", "value":34, "unit_id":"MeasurementUnit:code.mm",},
		],
	"cage_stays":[
		"CageStay:start_date.2016,10,18&&cage_id.Cage:id.27",
		"CageStay:cage_id.Cage:id.36",
		"CageStay:start_date.2017,1,4,16,30&&cage_id.Cage:id.27",
		],
	})

add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"f", "ear_punches":"L",
	"license":"666/2013",
	"birth_date":"2016,7,21",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5669"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M2885"},
		],
	"genotypes":["Genotype:code.eptg"],
	"biopsies":[
		{"CATEGORY":"BrainBiopsy","start_date":"2016,11,14,16",
			"extraction_protocol_id":"Protocol:code.mpm","sectioning_protocol_id":"Protocol:code.vt200",
			"sample_location":"w1",
			},
		],
	"measurements":[
		{"CATEGORY":"WeightMeasurement","date":"2016,10,27,22,18","weight":20.1, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,10,27,22,18","weight":19.5, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,10,27,22,18,22",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"stimulations":["StimulationProtocol:code.chr_longSOA"],
			"temperature":36.3,
			"irregularities":["Irregularity:description.ICA failed to indicate response to stimulus"],
			},
		],
	"observations":[
		{"CATEGORY":"Observation","date":"2016,11,17,14,15","physiology":"tail remainder", "value":10, "unit_id":"MeasurementUnit:code.mm",},
		],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.26",
		],
	})

add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"f", "ear_punches":"R",
	"license":"666/2013",
	"birth_date":"2016,7,21",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5670"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M2886"},
		],
	"genotypes":["Genotype:code.eptg"],
	"biopsies":[
		{"CATEGORY":"BrainBiopsy","start_date":"2016,11,14,16",
			"extraction_protocol_id":"Protocol:code.mpm","sectioning_protocol_id":"Protocol:code.vt200",
			"sample_location":"w2",
			},
		],
	"measurements":[
		{"CATEGORY":"WeightMeasurement","date":"2016,10,27,23,35","weight":21.4, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,4,17,30","weight":21.0, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,10,27,23,35,52",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"stimulations":["StimulationProtocol:code.chr_longSOA"],
			"temperature":36.1,
			"irregularities":["Irregularity:description.ICA failed to indicate response to stimulus"],
			},
		],
	"observations":[
		{"CATEGORY":"Observation","date":"2016,11,17,14,15","physiology":"tail remainder", "value":10, "unit_id":"MeasurementUnit:code.mm",},
		],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.26",
		],
	})

add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"f", "ear_punches":"LR",
	"license":"666/2013",
	"birth_date":"2016,7,21",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5671"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M2888"},
		],
	"genotypes":["Genotype:code.epwt"],
	"measurements":[
		{"CATEGORY":"WeightMeasurement","date":"2016,10,28,1,0","weight":20.7, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,4,17,30","weight":20.9, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,10,19,29","weight":21.3, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,17,14,15","weight":21.7, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,24,13,10","weight":22.5, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,10,28,1,0,22",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"stimulations":["StimulationProtocol:code.chr_longSOA"],
			"temperature":34.8,
			"irregularities":["Irregularity:description.ICA failed to indicate response to stimulus"],
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,11,10,19,29,49",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"stimulations":["StimulationProtocol:code.jb_long"],
			"temperature":34.7,
			"irregularities":["Irregularity:description.ICA failed to indicate response to stimulus"],
			},
		],
	"observations":[
		{"CATEGORY":"Observation","date":"2016,11,17,14,15","physiology":"tail remainder", "value":30, "unit_id":"MeasurementUnit:code.mm",},
		],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.26",
		],
	})

add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"f", "ear_punches":"R",
	"license":"666/2013",
	"birth_date":"2016,7,15",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5676"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M2912"},
		],
	"genotypes":["Genotype:code.eptg"],
	"measurements":[
		{"CATEGORY":"WeightMeasurement","date":"2016,10,28,2,25","weight":23.6, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,4,17,30","weight":23.8, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,10,21,9","weight":22.4, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,17,14,15","weight":24.6, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2016,11,24,14,2","weight":25.4, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,10,28,2,25,2",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"stimulations":["StimulationProtocol:code.chr_longSOA"],
			"temperature":36.5,
			"irregularities":["Irregularity:description.ICA failed to indicate response to stimulus"]
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,11,10,21,9,34",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"stimulations":["StimulationProtocol:code.chr_longSOA"],
			"temperature":34.8,
			"irregularities":["Irregularity:description.ICA failed to indicate response to stimulus"]
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,11,24,14,2,41",
			"anesthesia_id":"AnesthesiaProtocol:code.MP200scIi",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"temperature":35.7,
			},
		],
	"observations":[
		{"CATEGORY":"Observation","date":"2016,11,17,14,15","physiology":"tail remainder", "value":36, "unit_id":"MeasurementUnit:code.mm",},
		],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.28",
		],
	})

add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"m", "ear_punches":"L",
	"license":"666/2013",
	"birth_date":"2016,6,1",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5677"},
		],
	# "genotypes":["Genotype:code.datg"],?
	"measurements":[
		],
	"observations":[
		],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.50",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"m", "ear_punches":"R",
	"license":"666/2013",
	"birth_date":"2016,6,1",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5678"},
		],
	# "genotypes":["Genotype:code.datg"],?
	"measurements":[
		],
	"observations":[
		],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.50",
		],
	})
add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"m", "ear_punches":"LR",
	"license":"666/2013",
	"birth_date":"2016,6,1",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5679"},
		],
	# "genotypes":["Genotype:code.datg"],?
	"measurements":[
		],
	"observations":[
		],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.50",
		],
	})
