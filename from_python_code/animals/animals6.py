from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'
add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"f", "ear_punches":"R",
	"license":"666/2013",
	"birth_date":"2016,7,21",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5681"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M2898"},
		],
	"genotypes":["Genotype:code.datg"],
	"measurements":[
		{"CATEGORY":"WeightMeasurement","date":"2016,12,21,14,27","weight":23.8, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,12,21,14,27,52",
			"anesthesia_id":"AnesthesiaProtocol:code.MP200scIi",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.cryo_support",
			"stimulations":[
				"StimulationProtocol:code.jp_phasic",
				"StimulationProtocol:code.chr_longSOA",
				],
			"temperature":36,
			"irregularities":[
				"Irregularity:description.overadministered medetomidine (2x)",
				"Irregularity:description.implant detached during measurement",
				"Irregularity:description.ICA failed to indicate response to stimulus",
				],
			},
		],
	"observations":[
		],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.30",
		],
	})

add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"m", "ear_punches":"L",
	"license":"666/2013",
	"birth_date":"2016,7,21",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5682"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M2889"},
		],
	"genotypes":["Genotype:code.datg"],
	"measurements":[
		{"CATEGORY":"WeightMeasurement","date":"2016,12,22,13,35","weight":29.6, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,3,30,11,48","weight":30.2, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2016,12,22,13,35,49",
			"anesthesia_id":"AnesthesiaProtocol:code.Ifb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"stimulations":[
				],
			"temperature":35,
			"irregularities":[
				"Irregularity:description.measurement aborted"
				],
			},
		{
			"CATEGORY":"FMRIMeasurement",
			"date":"2017,3,30,11,48,52",
			"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
			"operator_id":"Operator:code.YPI",
			"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
			"stimulations":[
				"StimulationProtocol:code.chr_longSOA",
				],
			"temperature":35.7,
			"irregularities":[
				"Irregularity:description.50%% less oxygen than protocol"
				],
			},
		],
	"observations":[
		],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.31",
		"CageStay:cage_id.Cage:id.37",
		],
	})

add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"m", "ear_punches":"R",
	"license":"666/2013",
	"birth_date":"2016,7,21",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5683"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M2890"},
		],
	"genotypes":["Genotype:code.datg"],
	"observations":[
		],
	"measurements":[
		{"CATEGORY":"WeightMeasurement","date":"2017,3,30,13,14","weight":32.3, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
			{
				"CATEGORY":"FMRIMeasurement",
				"date":"2017,3,30,13,14,33",
				"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
				"operator_id":"Operator:code.YPI",
				"stimulations":[
					"StimulationProtocol:code.chr_longSOA",
					],
				"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
				"temperature":35.7,
				},
		],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.31",
		],
	})

add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"m", "ear_punches":"LR",
	"license":"666/2013",
	"birth_date":"2016,7,21",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5684"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M2891"},
		],
	"genotypes":["Genotype:code.eptg"],
	"observations":[
		],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.31",
		],
	})

add_generic(db_location, parameters={"CATEGORY":"Animal", "sex":"m", "ear_punches":"2L",
	"license":"666/2013",
	"birth_date":"2016,7,21",
	"external_ids":[
		{"CATEGORY":"AnimalExternalIdentifier","database":"ETH/AIC","identifier":"5685"},
		{"CATEGORY":"AnimalExternalIdentifier","database":"UZH/iRATS","identifier":"M2892"},
		],
	"genotypes":["Genotype:code.datg"],
	"measurements":[
		{"CATEGORY":"WeightMeasurement","date":"2016,12,22,18,19","weight":31, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
		{"CATEGORY":"WeightMeasurement","date":"2017,3,30,14,44","weight":29.1, "weight_unit_id":"MeasurementUnit:code.g", "operator_id":"Operator:code.YPI"},
			{
				"CATEGORY":"FMRIMeasurement",
				"date":"2016,12,22,18,19,41",
				"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
				"operator_id":"Operator:code.YPI",
				"stimulations":[
					"StimulationProtocol:code.jp_phasic",
					"StimulationProtocol:code.chr_longSOA",
					],
				"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
				"temperature":36.4,
				"irregularities":["Irregularity:description.ICA failed to indicate response to stimulus"],
				},
			{
				"CATEGORY":"FMRIMeasurement",
				"date":"2017,3,30,14,44,52",
				"anesthesia_id":"AnesthesiaProtocol:code.MscIfb",
				"operator_id":"Operator:code.YPI",
				"stimulations":[
					"StimulationProtocol:code.jp_phasic",
					"StimulationProtocol:code.chr_longSOA",
					],
				"scanner_setup_id":"FMRIScannerSetup:code.basic_ofm",
				"temperature":35.4,
				"irregularities":["Irregularity:description.ICA failed to indicate response to stimulus"],
				},
		],
	"observations":[
		],
	"cage_stays":[
		"CageStay:cage_id.Cage:id.31",
		],
	})
