from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'
append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5698",
	parameters={"death_date":"2017,4,19,15", "death_reason":"unknown",})
append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6261",
	parameters={"death_date":"2017,4,9,18,35", "death_reason":"unknown, possibly angled implant",})
append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6258",
	parameters={"death_date":"2017,4,20,18,30", "death_reason":"uknown",})
append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5682",
	parameters={"death_date":"2017,5,13,17,25", "death_reason":"end of experiment",})
append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5685",
	parameters={"death_date":"2017,5,23,19,0", "death_reason":"implant detached, euthanized",})
append_parameter(db_location, entry_identification='Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.6255',
	parameters={"death_date":"2017,5,26,18,20", "death_reason":"unknown",})
