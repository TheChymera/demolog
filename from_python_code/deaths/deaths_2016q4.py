from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'
append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5681",
	parameters={"death_date":"2016,12,21", "death_reason":"euthanized",})

append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5672",
	parameters={"death_date":"2016,11,24", "death_reason":"possibly contaminated fluoxetine",})

append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5669",
	parameters={"death_date":"2016,11,4", "death_reason":"brain extraction for immunohistochemistry",})
append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5670",
	parameters={"death_date":"2016,11,4", "death_reason":"brain extraction for immunohistochemistry",})
