from labbookdb.db.add import add_generic, append_parameter
db_location = 'meta.db'
append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5674",
	parameters={"death_date":"2017,2,3", "death_reason":"euthanized",})

append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5695",
	parameters={"death_date":"2017,1,16","death_reason":"euthanised",})
append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5696",
	parameters={"death_date":"2017,1,27","death_reason":"end of experiment",})
append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5697",
	parameters={"death_date":"2017,1,24","death_reason":"killed by littermate",})

append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5686",
	parameters={"death_date":"2017,1,23", "death_reason":"unknown",})
append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5687",
	parameters={"death_date":"2017,1,23", "death_reason":"killed by littermates",})
append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5688",
	parameters={"death_date":"2017,1,23", "death_reason":"unknown",})

append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5671",
	parameters={"death_date":"2017,1,13","death_reason":"end of experiment",})
append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5675",
	parameters={"death_date":"2017,2,13,15","death_reason":"end of experiment",})
append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5676",
	parameters={"death_date":"2017,1,13","death_reason":"end of experiment",})

append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5701",
	parameters={"death_date":"2018,3,31","death_reason":"end of experiment",})
append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5702",
	parameters={"death_date":"2018,3,31","death_reason":"end of experiment",})
append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5703",
	parameters={"death_date":"2018,3,31","death_reason":"end of experiment",})

append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5704",
	parameters={"death_date":"2018,3,31","death_reason":"end of experiment",})
append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5705",
	parameters={"death_date":"2018,3,31","death_reason":"end of experiment",})
append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5706",
	parameters={"death_date":"2018,3,31","death_reason":"end of experiment",})

append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5689",
	parameters={"death_date":"2018,3,31","death_reason":"end of experiment",})
append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5690",
	parameters={"death_date":"2018,3,31","death_reason":"end of experiment",})
append_parameter(db_location, entry_identification="Animal:external_ids.AnimalExternalIdentifier:database.ETH/AIC&#&identifier.5691",
	parameters={"death_date":"2018,3,31","death_reason":"end of experiment",})
