LabbookDB Logging Examples
==========================

This repository contains example logging input for LabbookDB_.

|
Disclaimer
----------

The data logged herein is loosely based on the experimental structure of wet work performed by TheChymera_.
It has been significantly modified to showcase LabbookDB's features, and is not monitored, updated, or curated for any other purposes
It should not be used in any way as a reference for experiments actually performed.

|
Installation
------------

The code is best installed directly via Git:

.. code-block:: console

  git clone https://bitbucket.org/TheChymera/demolog
And set yourself up for following the usage examples:

.. code-block:: console

  cd logging_examples

|
Usage
-----

|
From Python Code
~~~~~~~~~~~~~~~~

Run from the root of the repository clone:

.. code-block:: console

  cd from_python_code
  ./generate_db.py

|
Move Database to LabbookDB Example Default
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: console

  cp meta.db ~/syncdata/meta.db

|
Dependencies
------------

* LabbookDB_

|
License
-------

`GNU General Public License Version 3`__

__ https://bitbucket.org/TheChymera/logging_examples/raw/49b637f9db2463d2b1bb0f482199d0e02604e9b0/LICENSE.txt
.. _LabbookDB: https://github.com/TheChymera/LabbookDB
.. _TheChymera: https://github.com/TheChymera